let log = '';
const path = '';
const initialDataFolder = 'askue-mobile';

/**
 * Метод получения функции обработчика файла
 */
function getFileHandler(){

  // LVM 01.03.2121 Для того, чтобы сохранять файл в видимой части
  //  хранилища для соединения с ЭВМ используем cordova.file.externalApplicationStorageDirectory
  //  YourComputer\CellPhoneName\Internal shared storage\Android\data\app.locataion\yourFile.ext
  this.path = cordova.file.externalApplicationStorageDirectory;
  window.resolveLocalFileSystemURL(cordova.file.externalApplicationStorageDirectory, fileHandlerSuccess, fileHandlerError);
}

/**
 * Коллбэк успешного получения обработчика(экземпляра) файлового хранилища (Здесь создаем папку)
 * @param folder
 */
function fileHandlerSuccess(folder) {
  // LVM 17.03.2021 Create default folder on first run or create file with current date and time
  checkFolderExists(folder);
}

/**
 * LVM 17.03.2021 Check if folder exists
 * @param path
 * @param
 * @returns {boolean}
 */
function checkFolderExists(folder) {
  window.resolveLocalFileSystemURL(path + initialDataFolder, folderExists(folder), createFolder(folder));
}

/**
 * Create folder if not exists(basically on app first run)
 */
function createFolder(folder) {
  // todo code here ...
  // LVM 02.03.2021 Создаем папку
  console.log('CREATING FOLDER <<<<<<<!!!!@@@@@@##########$$$$$$$$$$');
  folder.getDirectory(initialDataFolder, { create: true}, createFolderHandler, e1);
}

function e1(e){
  console.log('EGGOG e1 !!!!!!!!!!!!!!!!!!!!!!!');
  console.log(e);
}

/**
 * Do nothing if folder exists
 */
function folderExists(folder) {
  // todo do nothing code here ...
  console.log(folder);
  console.log('>>> Skipping CREATING FOLDER');
  folder.getDirectory(initialDataFolder, { create: false}, createFolderHandler, e2);
}

function e2(e){
  console.log('EGGOG e2 !!!!!!!!!!!!!!!!!!!!!!!');
  console.log(e);
}

/**
 * Коллбэк успешного получения обработчика(экземпляра) созданной папки
 * @param newFolder
 */
function createFolderHandler(newFolder){
  console.log("function createFolderHandler(newFolder)");
  // return;

  let now = new Date();
  let y  = now.getFullYear();
  let m  = now.getMonth() + 1;
  let d  = now.getDate();
  let hh = now.getHours();
  let mm = now.getMinutes();
  let ss = now.getSeconds();
  let ms = now.getMilliseconds();

  let jsonFilename = d + '_' + m + '_' + y + '_' + hh + '_' + mm + '_' + ss + '_' + ms + '.json';
  console.log(jsonFilename);
  newFolder.getFile(jsonFilename, { create: true }, createFileHandlerLogFile);
}

/**
 * Коллбэк ошибки получения обработчика файла
 * @param dir
 */
function fileHandlerError(e) {
  console.log("function fileHandlerError(e)");
  console.log(e);
}

/**
 * Создать обработчик создания файла журналирования
 * @param file
 */
function createFileHandlerLogFile(file) {
  console.log("got the file", file);
  writeFileHandlerLog("App started");
}

/**
 * Метод логирования создания файла
 * @param str
 */
function writeFileHandlerLog(str) {
  if(!logOb) return;
  log = str + " [" + (new Date()) + "]\n";
  console.log("going to log " + log);
  logOb.createWriter(fileHandlerLogSuccess, fileHandlerLogError);
}

/**
 * Коллбэк успешной отработки метода логирования создания файла
 * @param fileWriter
 */
function fileHandlerLogSuccess(fileWriter) {
  fileWriter.seek(fileWriter.length);

  var blob = new Blob([log], {type:'text/plain'});
  fileWriter.write(blob);
  console.log("ok, in theory i worked");
}

/**
 * Коллбэк ошибки обработки метода логирования создания файла
 * @param fileWriter
 */
function fileHandlerLogError(fileWriter) {
  fileWriter.seek(fileWriter.length);

  var blob = new Blob([log], {type:'text/plain'});
  fileWriter.write(blob);
  console.log("ok, in theory i worked");
}

// ---------------------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------

// function f1(imgLink) {
//
//   // window.resolveLocalFileSystemURL(cordova.file.cacheDirectory, function success(dirEntry) {
//   //
//   //   // Создаем файл, куда будем сохранять файл
//   //   dirEntry.getFile(fileNameVar, { create: true, exclusive: false }, function (fileEntry) {
//   //
//   //     // дотягиваемся до файла снимка
//   //     resolveLocalFileSystemURL(imgLink, function(fentry) {
//   //
//   //       fentry.file(function (file) {
//   //         var reader = new FileReader();
//   //
//   //         reader.onloadend = function() {
//   //           var base64 = btoa(
//   //               new Uint8Array(this.result)
//   //                   .reduce((data, byte) => data + String.fromCharCode(byte), '')
//   //           );
//   //
//   //           writeFile(fileEntry, base64, fileNameVar, fentry);
//   //         };
//   //
//   //         reader.readAsArrayBuffer(file);
//   //
//   //       }, fsError);
//   //     });
//   //
//   //     return;
//   //
//   //   }, fsError);
//   //
//   // }, fsError);
// }
//
// function f2(fileEntry, dataObj, fileNameVar, fentry) {
//   // Create a FileWriter object for our FileEntry (log.txt).
//   // fileEntry.createWriter(function (fileWriter) {
//   //
//   //   // console.log(fileEntry);
//   //
//   //   fileWriter.onwriteend = function() {
//   //
//   //     // console.log("Successful file write...");
//   //     readFile(fileEntry);
//   //     var uri = encodeURI(localStorage.getItem('baseUrl') + '/api/photos/upload/one');
//   //     // console.log('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> upload URI : ' + uri);
//   //     // console.log(fileNameVar);
//   //     // console.log(fileEntry);
//   //
//   //     // uri = 'https://webhook.site/77d71de5-bb41-4987-b0a7-31f977850e39';
//   //
//   //     var options = new FileUploadOptions();
//   //     options.fileKey="file";
//   //     options.fileName='/data/data/com.system.location/cache/' + fileNameVar
//   //     options.mimeType="image/png";
//   //     options.httpMethod='POST';
//   //     options.accept='application/json';
//   //     options.chunkedMode=false;
//   //
//   //     var params = {};
//   //     params.createdAt = fileNameVar.replace('.png','');
//   //     var ta = params.createdAt.split('T')
//   //     var d1 = ta[0].split('_'); //dd mm yyyy
//   //     var dfinal = d1[2] + '-' + d1[1] + '-' + d1[0];
//   //
//   //     var t1 = ta[1].split('_'); //hh mm ss
//   //     var h1 = parseInt([t1[0]]) < 10 ? '0' + t1[0] : t1[0];
//   //     var m1 = parseInt([t1[1]]) < 10 ? '0' + t1[1] : t1[1];
//   //     var s1 = parseInt([t1[2]]) < 10 ? '0' + t1[2] : t1[2];
//   //     var tfinal = h1 + ':' + m1 + ':' + s1 + '.000+03:00';
//   //     var dtfinal = dfinal + 'T' + tfinal;
//   //
//   //     params.createdAt = dtfinal;
//   //
//   //     // console.log(params.createdAt);
//   //
//   //     params.planId = localStorage.getItem('worklist_id');
//   //     params.workListId = localStorage.getItem('worklist_id');
//   //
//   //     options.params = params;
//   //
//   //     var ft = new FileTransfer();
//   //     ft.upload('/data/data/com.system.location/cache/' + fileNameVar, uri, function(r){
//   //       // console.log(r);
//   //       // console.log("Code = " + r.responseCode);
//   //       // console.log("Response = " + r.response);
//   //       // console.log("Sent = " + r.bytesSent);
//   //       // fentry.remove(function (file) { console.log(file.name + ' has been removed.'); },
//   //       //     function(error) { console.log(error); },
//   //       //     function(){ console.log('File does not exist'); });
//   //     }, function(error){ console.log(error); }, options);
//   //
//   //   };
//   //
//   //   fileWriter.onerror = function (e) {
//   //     console.log("Failed file write: " + e.toString());
//   //   };
//   //
//   //   dataObj = dataObj.replace("data:image/jpeg;base64,", '');
//   //   var blob = b64toBlob(dataObj, 'image/png');
//   //   fileWriter.write(blob);
//   // });
// }
//
// function f3(fileEntry) {
//
//   // fileEntry.file(function (file) {
//   //   var reader = new FileReader();
//   //
//   //   reader.onloadend = function() {
//   //
//   //     // console.log("Successful file read: " + this.result);
//   //     var base64 = btoa(
//   //         new Uint8Array(this.result)
//   //             .reduce((data, byte) => data + String.fromCharCode(byte), '')
//   //     );
//   //
//   //     $('#objectPic' + iterator_img).empty();
//   //     $('#objectPic' + iterator_img).attr("src", 'data:image/jpeg;base64,' + base64);
//   //     $('#picContainer' + iterator_img).attr('hidden', false);
//   //   };
//   //
//   //   reader.readAsArrayBuffer(file);
//   // }, fsError);
// }