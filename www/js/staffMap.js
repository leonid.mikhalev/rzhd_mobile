var appMap;
var latitude, longitude;



/**
 * Метод отслеживающий загрузку ДОМа
 */
$(document).ready(function() {




    appMap = L.map('mapid').setView([56.449497, 36.566990], 12);
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(appMap);

    appMap.on('click', appMapClick);

    var w = (window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth) - 50;
    var h = (window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight) - 300;
    var mapElement = document.getElementById('mapid');


    // Слои с федеративным делением на октябрьской жд
    var geojsonFeature;
    var rawFile = new XMLHttpRequest();
    rawFile.open("GET", 'Regions.geojson', false);
    rawFile.onreadystatechange = function ()
    {
        if(rawFile.readyState === 4)
        {
            if(rawFile.status === 200 || rawFile.status == 0)
            {
                geojsonFeature = rawFile.responseText;
                var geoJson = JSON.parse(geojsonFeature);
                // console.log(geoJson);

                L.geoJSON(geoJson, {
                    style: function(Feature) {
                        switch (Feature.properties.NAME) {
                            case 'Мурманская область':    return {color: "#ff9b6d"};
                            case 'Псковская область':     return {color: "#f3a080"};
                            case 'Ленинградская область': return {color: "#a890c4"};
                            case 'Тверская область':      return {color: "#b3ba9b"};
                            case 'Вологодская область':   return {color: "#fdf973"};
                            case 'Новгородская область':  return {color: "#c5d147"};
                            case 'Архангельская область': return {color: "#62b9a6"};
                            case 'Республика Карелия':    return {color: "#bba576"};
                            case 'Москва':                return {color: "#fffb79"};
                            case 'Санкт-Петербург':       return {color: "#a890c4"};
                            case 'Московская область':    return {color: "#fffb79"};
                        }
                    }
                }).addTo(appMap);
            }
        }
    }
    rawFile.send(null);

    // var geojsonFeature2 = '{"type": "Feature","properties": {"name": "Coors Field","amenity": "Baseball Stadium","popupContent": "This is where the Rockies play!"},"geometry": {"type": "CircleMarker","coordinates": [56.449497, 36.566990]}}';
    // var geoJsonSt = JSON.parse(geojsonFeature2);
    // L.geoJSON(geoJsonSt).addTo(appMap);

    // Слои - точки со станциями
    var railMapStats;
    var st_geo;
    var geojsonStations;
    var rawFileStation = new XMLHttpRequest();
    rawFileStation.open("GET", 'ozd.geojson', false);
    rawFileStation.onreadystatechange = function ()
    {
        if(rawFileStation.readyState === 4)
        {
            if(rawFileStation.status === 200 || rawFileStation.status == 0)
            {
                geojsonStations = rawFileStation.responseText;
                var geoJsonSt = JSON.parse(geojsonStations);
                // console.log(geoJsonSt);

                var geojsonMarkerOptions = {
                    radius: 3,
                    fillColor: "#ff7800",
                    color: "#000",
                    weight: 1,
                    opacity: 1,
                    fillOpacity: 0.8
                };

                railMapStats = L.geoJSON(geoJsonSt, {
                    pointToLayer: function (feature, latlng) {
                        return L.circleMarker(latlng, geojsonMarkerOptions)
                    }
                }).addTo(appMap);
                //
                // var conCoords = connectTheDots(railMapStats);
                // var conLine = L.polyline(conCoords).addTo(appMap)

            }
        }
    }
    rawFileStation.send(null);


    // var st_ozd;
    // var geojsonStations2;
    // var rawFileStation2 = new XMLHttpRequest();
    // rawFileStation2.open("GET", 's.json', false);
    // rawFileStation2.onreadystatechange = function ()
    // {
    //     if(rawFileStation2.readyState === 4)
    //     {
    //         if(rawFileStation2.status === 200 || rawFileStation.status == 0)
    //         {
    //             geojsonStations2 = rawFileStation2.responseText;
    //             var geoJsonSt2 = JSON.parse(geojsonStations2);
    //             st_ozd = geoJsonSt2;
    //             // console.log(geoJsonSt2);
    //             // var strNeeded = '[';
    //             // $.each(geoJsonSt, function(i, item) {
    //             //     if ( item.railway_name.toLocaleLowerCase().includes('октябрьская') ) {
    //             //         // console.log('i = ' + i + ', item = ' + item.railway_name + ', json = ' + JSON.stringify(item));
    //             //         strNeeded += JSON.stringify(item) + ',';
    //             //     }
    //             //     // if (i > 10) return false;
    //             // });
    //             // strNeeded += ']';
    //             // console.log(strNeeded)
    //
    //
    //         }
    //     }
    // }
    // rawFileStation2.send(null);

    // console.log(st_geo.featrures);
    // var s_ozd = '[';
    // $.each(st_ozd, function(i, item) {
    //     $.each(st_geo.features, function(i_geo, item_geo) {
    //         // console.log(item_geo.properties.esr);
    //         if ( item.st_code == item_geo.properties.esr) {
    //             s_ozd += JSON.stringify(item_geo) + ',';
    //         }
    //     });
    //
    //     // if ( item.railway_name.toLocaleLowerCase().includes('октябрьская') ) {
    //         // console.log('i = ' + i + ', item = ' + item.railway_name + ', json = ' + JSON.stringify(item));
    //     // }
    //     // if (i > 10) return false;
    // });
    // s_ozd += ']'
    //
    // // console.log(st_geo);
    // // console.log(st_ozd);
    //
    // console.log(s_ozd);



});

function connectTheDots(data){
    var c = [];
    for(i in data._layers) {
        var x = data._layers[i]._latlng.lat;
        var y = data._layers[i]._latlng.lng;
        c.push([x, y]);
    }
    return c;
}

document.addEventListener("DOMContentLoaded", () => {});

function appMapClick(e){
    var lat = e.latlng.lat;
    var lng = e.latlng.lng;
    var popLocation= e.latlng;
    var popup = L.popup()
        .setLatLng(popLocation)
        .setContent("Выбранная точка<br /> <hr /> Широта: " + lat + "<br /> Долгота: " + lng)
        .openOn(appMap);

    addGeoPoint(lat, lng);


}


function addGeoPoint(lat, lng) {
    var statusId = Math.floor(9.0 * Math.random());
    var body = '{\n' +
        '    "id": 1,\n' +
        '    "team": { "id": 2 },\n' +
        '    "status": { "id": ' + statusId + ' },\n' +
        '    "location": "[' + lat + ', ' + lng + ']"\n' +
        '}\n';

    // console.log(statusId);
    // console.log(body);

    var xhr = new XMLHttpRequest();
    var responseJson = "";
    xhr.open('POST', 'http://192.168.2.40:8080/api/team/history', true);
    xhr.onreadystatechange = function() {
        if (xhr.readyState === 2) {}
    }
    xhr.setRequestHeader('Access-Control-Allow-Origin', '*');
    xhr.setRequestHeader('accept', 'application/json');
    xhr.setRequestHeader('Content-Type', 'application/json');

    xhr.onload = function() {
        responseJson = xhr.response;
        // console.log(responseJson);
        //var json = JSON.parse(responseJson);
        //console.log(responseJson);
    };
    xhr.onprogress = function(event) {};
    xhr.onloadend = function (ev) {}
    xhr.onerror = function (ev) {}
    xhr.send(body);
}


/** Получаем геолокацию **/
function getLocation(){
    return navigator.geolocation.getCurrentPosition(onLocSuccess, onLocError, { enableHighAccuracy: true });
    //watchPosition

}

/** Callback геолокации **/
var onLocSuccess = function(position) {

    var element = document.getElementById('posData');
    element.innerHTML = 'Широта: ' + position.coords.latitude          + '<br>' +
        'Долгота: '                + position.coords.longitude         + '<br>';
    this.latitude = position.coords.latitude;
    this.longitude = position.coords.longitude;
    //console.log('latitude = ' + latitude + ', longitude = ' + longitude);
    // appMap.panTo(new L.LatLng(latitude, longitude));
    // 1 zoom out -- 15 zoom in
    appMap.flyTo(new L.LatLng(latitude, longitude), 18);

    var c1 = L.circle([latitude, longitude], {
        color: 'yellow',
        fillColor: '#f03',
        fillOpacity: 0.5,
        radius: 20
    }).addTo(appMap).on("click", myPosClick );

};

/** Callback геолокации **/
function onLocError(error) {
    alert('code: '    + error.code    + '\n' +
        'message: ' + error.message + '\n');
}


function myPosClick(e){
    var lat = e.latlng.lat;
    var lng = e.latlng.lng;
    var myLocPopup = L.popup().setLatLng([lat, lng]);

    myLocPopup.setContent("Мое текущее местоположение<br /> <hr /> Широта: " + lat + "<br /> Долгота: " + lng);

    var clickedCircle = e.target;
    clickedCircle.bindPopup(myLocPopup).openPopup();
}


/*


*/