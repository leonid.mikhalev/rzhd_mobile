/**
 * Класс обработки событий страницы "ПОДКЛЮЧЕНИЯ"
 */
let testDev = true; // Тестовая переменная. Если правда - работаем с сервисом Романа, если ложь - с АПИ Сергея

const initialDataFolder = 'askue-mobile';
const app_con_log = 'feeder.json';

let img_ConCounter = 0;

let wl_id = -1;   // Идентификаор плана
let wlo_id = -1;
let connId = -1;
let conn_name = '';
let device_type = '';
let conn_sts_msg = '';
let conn_status = null;

let conn_val = '';
let conn_comment = '';

let path = '';
let imgData= '';

let fConnFiles = [];

let selected_conn_json = '';

let connSndJsonData = '{"data" : []}';

let plan_details_Id = -1;
let planImages = [];

document.addEventListener('deviceready', function(event){
  path = cordova.file.externalApplicationStorageDirectory;
}, false);

$(document).ready(function() {

  $('#cover').hide();
  wl_id  = localStorage.getItem('worklist_id');


  console.warn(wl_id);

  var item = localStorage.getItem('worklist_item');
  var jsonItem = decodeURI(atob(item));
  console.info('%c[ITEM PASSED TO CONNECTION FORM]:\n%O', 'color: orangered;', JSON.parse(jsonItem));
  var connData = JSON.parse(jsonItem);
  let link = localStorage.getItem('baseUrl');


  let responseJson = "";
  let xhr = new XMLHttpRequest();

  var urlOutput = link + '/api/plan_details/' + connData.id;
  plan_details_Id = connData.id;

  xhr.open('GET', link + '/api/plan_details/' + connData.id, true);
  console.log(link);
  xhr.setRequestHeader('Access-Control-Allow-Origin', '*');
  xhr.setRequestHeader('accept', 'application/json');
  xhr.setRequestHeader('Content-Type', 'application/json');
  xhr.onload = function() {

    responseJson = xhr.response;
    console.log(JSON.parse(responseJson));
    var reqRespData = JSON.parse(responseJson);

    console.info('%c[DATA ACQUIRED]: \nurlOutput: %O\n%O','color: lime;', urlOutput, reqRespData);

    $('#cover').hide();

    document.getElementById('connName').innerHTML = 'Наименование фидера: <b>' + reqRespData.device.name + '</b>';
    document.getElementById('connType').innerHTML = 'Тип счетчика: <b>' + reqRespData.device.deviceType.name + '</b>';
    document.getElementById('connFactoryNum').innerHTML = 'Номер счетчика: <b>' + reqRespData.device.factoryNumber + '</b>';
    document.getElementById('tConnValue').value = reqRespData.measurement === null ? '' :  reqRespData.measurement;
    document.getElementById('tConnComment').value = reqRespData.remark === null ? '' :  reqRespData.remark;

    checkSessionPhotos();




  };
  xhr.onprogress = function(event) {};
  xhr.onloadend = function (ev) {};
  xhr.onerror = function (ev) {
    $('#cover').hide();
    console.log('%cERROR\n%O', 'color:red;', ev);
  };
  xhr.send();
});


function checkSessionPhotos() {

  // -----------------------------------------------------------------------------------------------------------------
  // -----------------------------------------------------------------------------------------------------------------
  // -----------------------------------------------------------------------------------------------------------------

  var planDetailsItemsJsonDecoded = null;

  try {

    planDetailsItemsJsonDecoded = JSON
        .parse(
            decodeURI(
                atob(
                    localStorage.getItem("planDetailsItemsJsonEncoded"))
            )
        );
    console.info('%c [IMAGE DATA FROM STORAGE]\n%O', 'color: #e6e872;', planDetailsItemsJsonDecoded);

    $('#tConnLogPics').empty();
    fConnFiles = [];
    img_ConCounter = 0;
    planImages = planDetailsItemsJsonDecoded;

    if ( planImages ) {
      let imgTmplt = '';
      $.each(planImages, function (arrIndex, data) {
        console.info('%cplanId: %O; data: %O', 'color: orangered;', arrIndex, data);
        if ( data.plan_details_Id == plan_details_Id ) {
          console.info('%cdata.data: %O', 'color: orangered;', data.data);

          var countDataArr = data.data;
          // countDataArr.sort();
          countDataArr.sort((a,b) => {
            if ( b.imgCount < a.imgCount ) return -1;
            if ( b.imgCount > a.imgCount ) return 1;
            return 0;
          });
          console.info('%ccountDataArr: %O', 'color: red;', countDataArr);
          console.info('%ccountDataArr[0].imgCount: %O', 'color: red;', countDataArr[0].imgCount);
          img_ConCounter = countDataArr[0].imgCount + 1;

          // /sdcard//Android/data/com.system.location/askue-mobile/15_7_2021_15_48_21.596.png
          $.each(data.data, function (dataIndex, dataValue) {
            console.info('%cimgCounter: %O; fileSrc: %O', 'color: orangered;', dataValue.imgCount, dataValue.src);

console.info('%c dataValue.src: %O', 'color: red;', dataValue.src);
            let cf = new ConnectionPhoto(dataValue.imgCount, dataValue.src);
            fConnFiles.push(cf); // Не забыть удалить из массива, в случае fileWriterError

            imgTmplt +=
                '<li class="list-group-item" id="li_con_' + dataValue.imgCount + '">\n' +
                '    <img class="card-img-top" src="' + dataValue.src + '">\n' +
                '    <div class="d-flex flex-row-reverse" style="padding-top: 10px;" id="img_' + dataValue.imgCount + '">\n' +
                '        <button type="button" class="btn btn-outline-danger" onclick="remFromConList(' + dataValue.imgCount + ')" style="margin-left: 3px;">Удалить</button>\n' +
                '    </div>\n' +
                '</li>\n';
          });

        }
      });
      $('#tConnMeterPics').append(imgTmplt);


    }

  } catch (e) {
    console.info('%c [IMAGE DATA FROM STORAGE] EMPTY', 'color: orangered;');
    console.info(e);
  }
  // -----------------------------------------------------------------------------------------------------------------
  // -----------------------------------------------------------------------------------------------------------------
  // -----------------------------------------------------------------------------------------------------------------

}


/**
 * Перенаправление на регламент
 */
function getRegulations() {
  localStorage.setItem('worklist_id' , wl_id);
  localStorage.setItem('wl_object_id', wlo_id);
  localStorage.setItem('conn_id'     , connId);

  $(location).attr('href','taskList.html');
}

/**
 * ДЛЯ РАЗРАБОТКИ
 * Местный вспомогательный метод получения строки из даты
 * @param par1 - delimiter
 * @returns {string}
 */
function getDate(par1) {
  let now = new Date();
  let y  = now.getFullYear();
  let m  = now.getMonth() + 1;
  let d  = now.getDate();
  let hh = now.getHours();
  let mm = now.getMinutes();
  let ss = now.getSeconds();
  let ms = now.getMilliseconds();

  let retVal =
      d  + par1 +
      m  + par1 +
      y  + par1 +
      hh + par1 +
      mm + par1 +
      ss + '.' + ms;

  return retVal;
}

/**
 * Коневертация BASE64 в бинарник
 * @param b64Data
 * @param contentType
 * @param sliceSize
 * @returns {Blob}
 */
function b64toBlob(b64Data, contentType, sliceSize) {
  contentType = contentType || '';
  sliceSize = sliceSize || 512;

  let byteCharacters = atob(b64Data);
  let byteArrays = [];

  for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
    let slice = byteCharacters.slice(offset, offset + sliceSize);

    let byteNumbers = new Array(slice.length);
    for (let i = 0; i < slice.length; i++) {
      byteNumbers[i] = slice.charCodeAt(i);
    }

    let byteArray = new Uint8Array(byteNumbers);

    byteArrays.push(byteArray);
  }

  let blob = new Blob(byteArrays, {type: contentType});
  return blob;
}

/**
 * Инициализация камеры
 */
function getPicture() {

  $('#cover').show();

  try {
    if ( cordova ) {
      console.log(cordova.plugins.permissions);

      let options = {
        quality: 100,
        destinationType: Camera.DestinationType.DATA_URL,
        sourceType: Camera.PictureSourceType.CAMERA,
        mediaType: Camera.MediaType.PICTURE,
        encodingType: Camera.EncodingType.JPG,
        cameraDirection: Camera.Direction.BACK,
        correctOrientation: 1,
        targetWidth: 1440,
        targetHeight: 2037
      };
      try {
        navigator.camera.getPicture(cameraSuccess, cameraError, options);
      } catch (e) {
        $('#cover').hide();
        console.info('%c[ERROR] Method::getJrnlPicture.\n%O', 'color: red;', e);
      }
    }
  } catch (e) {
    $('#cover').hide();

    console.info('%c[INFO] Method::getPicture. Cordova is not defined, probably no device application running', 'color: white;');
  }
}

// /**
//  * Инициализация камеры
//  */
// function getPicture() {
//   console.info(4);
//   $('#cover').show();
//
//   console.log(cordova.plugins.permissions);
//
//   let options = {
//     quality: 100,
//     destinationType: Camera.DestinationType.DATA_URL,
//     sourceType: Camera.PictureSourceType.CAMERA,
//     mediaType: Camera.MediaType.PICTURE,
//     encodingType: Camera.EncodingType.JPG,
//     cameraDirection: Camera.Direction.BACK,
//     correctOrientation: 1,
//     targetWidth: 1440,
//     targetHeight: 2037
//   };
//   navigator.camera.getPicture(cameraSuccess, cameraError, options)
// }

/**
 * Получение блоба снимка [SUCCESS CALLBACK]
 * @param imgSrc
 */
function cameraSuccess(imgSrc) {
  imgData = imgSrc;
  window.resolveLocalFileSystemURL(cordova.file.externalApplicationStorageDirectory, createFolder, folderExists);
  //window.requestFileSystem()
}

/**
 * Создние папки [SUCCESS CALLBACK]
 */
function createFolder(folder) {
  folder.getDirectory(initialDataFolder, { create: true}, createFile, e1);
}

/**
 * Создание папки [SUCCESS CALLBACK]
 */
function folderExists(folder) {
  folder.getDirectory(initialDataFolder, { create: false}, createFile, e2);
}

/**
 * Создание файла [SUCCESS CALLBACK]
 * @param folder
 */
function createFile(folder) {
  let file = getDate('_') + '.png';

  // lvm 09.04.2021 DEVELOPMENT HARDCODE FILENAME
  // file = 'victoria.png';

  folder.getFile(file, { create: true }, createFileSuccess, createFileError);
}

// /**
//  * Создание fileWriter'а [SUCCESS CALLBACK]
//  * @param file
//  */
// function createFileSuccess(file) {
//   let cf = new ConnectionPhoto(img_ConCounter, file);
//   fConnFiles.push(cf); // Не забыть удалить из массива, в случае fileWriterError
//
//   file.createWriter(fileWriterSuccess, fileWriterError);
// }


/**
 * Создание fileWriter'а [SUCCESS CALLBACK]
 * @param file
 */
function createFileSuccess(file) {
  let cf = new ConnectionPhoto(img_ConCounter, '/sdcard/'+file.fullPath);
  fConnFiles.push(cf); // Не забыть удалить из массива, в случае fileWriterError

  if ( planImages ) {
    var newPlanEntry = true;
    $.each(planImages, function (arrIndex, data) {
      console.info('%cplanIndex: %O; data: %O', 'color: orangered;', arrIndex, data);
      if ( data.plan_details_Id == plan_details_Id ) {
        newPlanEntry = false;
        var newImgEntry = true;
        console.info('%cdata.data: %O', 'color: orangered;', data.data);
        $.each(data.data, function (dataIndex, dataValue) {
          console.info('%cimgCounter: %O; fileSrc: %O', 'color: orangered;', dataValue.imgCount, dataValue.src);
          if ( dataValue.imgCount == img_ConCounter ) {
            var newImgEntry = false;
            dataValue.src = file.fullPath;
          }
        });
        if ( newImgEntry ) {
          data.data.push({ "imgCount": img_ConCounter, "src": '/sdcard/' + file.fullPath });
        }
      }
    });
    if ( newPlanEntry ) {
      var item = { "plan_details_Id": plan_details_Id, "data" : [{"imgCount": img_ConCounter, "src": '/sdcard/' + file.fullPath}]};
      planImages.push(item);
    }


  }
  console.info('%c[IMAGES DATA]:\n%O', 'color: yellow;', planImages);
  var planDetailsItemsJsonEncoded = btoa(encodeURI(JSON.stringify(planImages)));
  localStorage.setItem("planDetailsItemsJsonEncoded", planDetailsItemsJsonEncoded);
  // var jsonItem = decodeURI(atob(item));
  // var connData = JSON.parse(jsonItem);


  file.createWriter(fileWriterSuccess, fileWriterError);
}

/**
 * Запись изображения в файл [SUCCESS CALLBACK]
 * @param fileWriter
 */
function fileWriterSuccess(fileWriter) {
  console.log(fileWriter);
  fileWriter.seek(fileWriter.length);
  let blob = b64toBlob(imgData, 'image/png', 512);
  fileWriter.onwrite = function (e) {
    console.log("WRITE SUCCESS");
    console.log(e);
    updateConList();
    img_ConCounter ++;
  }
  fileWriter.write(blob);
}

/**
 * Добавить шаблон списка снимка прибора к форме
 */
function updateConList(){

  let imgTmplt = '';
  $.each(fConnFiles, function(i, item) {
    if ( item.id == img_ConCounter ) {
      imgTmplt +=
      '<li class="list-group-item" id="li_con_' + img_ConCounter + '">\n' +
      '    <img class="card-img-top" src="' + item.file + '">\n' +
      '    <div class="d-flex flex-row-reverse" style="padding-top: 10px;" id="img_' + img_ConCounter + '">\n' +
      '        <button type="button" class="btn btn-outline-danger" onclick="remFromConList(' + img_ConCounter + ')" style="margin-left: 3px;">Удалить</button>\n' +
      '    </div>\n' +
      '</li>\n';

      // lvm Retrieve file from fileEntry example
      // try {
      //   item.file.file(function(f){ //this does the trick
      //     console.log(f);
      //   });
      // }
      // catch(err) { console.error(err); }

    }
  });

  $('#tConnMeterPics').append(imgTmplt);
  $('#cover').hide();

}


/**
 * Удалить снимок прибора из списка
 * @param par
 */
function remFromConList(par) {
  // Удаляем из списка в DOM'е
  let elem = document.getElementById('li_con_' + par);
  elem.parentElement.removeChild(elem);

  console.info('%c[fConnFiles]:\n%O', 'color: red;', fConnFiles);

  // Удаляем из массива
  $.each(fConnFiles, function(i, item) {
    if ( item.id == par ) {
      const index = fConnFiles.indexOf(item);
      if ( index > -1 ) {
        fConnFiles.splice(index, 1); return false;
      }
    }
  });

  var planDetailsItemsJsonDecoded = JSON
      .parse(
          decodeURI(
              atob(
                  localStorage.getItem("planDetailsItemsJsonEncoded"))
          )
      );


  // console.info('%c []', 'color: lime;', );
  console.info('%c [planDetailsItemsJsonDecoded]\n%O', 'color: lime;', planDetailsItemsJsonDecoded);
  console.info('%c [wl_id]: %O', 'color: lime;', wl_id);
  console.info('%c [par]: %O', 'color: lime;', par);


  $.each(planDetailsItemsJsonDecoded, function(index, data) {
    console.info('%c --> index: %O; data.plan_details_Id: %O; data: %O', 'color: yellow;', index, data.plan_details_Id, data);
    if ( data.plan_details_Id == plan_details_Id ) {
      $.each(data.data, function(i, v) {
        console.info('%c -----> i: %O; v.imgCount: %O; v: %O', 'color: #eced9f;', i, v.imgCount, v);
        if ( v.imgCount == par ) {
          console.info('%c DELETING ENTRY: %O', 'color: red;', v);
          data.data.splice(i, 1); return false;
        }
      });
    }
  });
  planImages = planDetailsItemsJsonDecoded;
  console.info('%c[IMAGES DATA planItemsJsonDecoded]:\n%O', 'color: cyan;', planDetailsItemsJsonDecoded);
  console.info('%c[IMAGES DATA planItemsJsonDecoded]:\n%O', 'color: magenta;', planImages);
  var planDetailsItemsJsonEncoded = btoa(encodeURI(JSON.stringify(planImages)));
  localStorage.setItem("planDetailsItemsJsonEncoded", planDetailsItemsJsonEncoded);

}

// /**
//  * Удалить снимок прибора из списка
//  * @param par
//  */
// function remFromConList(par) {
//   // Удаляем из списка в DOM'е
//   let elem = document.getElementById('li_con_' + par);
//   elem.parentElement.removeChild(elem);
//
//   // Удаляем из массива
//   $.each(fConnFiles, function(i, item) {
//     if ( item.id == par ) {
//       const index = fConnFiles.indexOf(item);
//       if ( index > -1 ) {
//         fConnFiles.splice(index, 1);
//       }
//     }
//   });
// }

/**
 * Закрыть/Закрыть с замечаниями задание в подключении
 */
function closeConIssue(par) {

  var keys = Object.keys(localStorage);
  // console.info('%c [LOCAL STORAGE ITEMS] LocalStorage:\n%O', 'color: orange;', keys);
  // $.each(keys, function(k, v) {
  //   console.info('%O, %O', v, localStorage.getItem(v));
  // });
  var issueObject = { "id": null, "measurement": null, "remark": null };

  localStorage.setItem("worklist_id", wl_id);
  localStorage.setItem('worklist_frcon_id', wl_id);

  // wl_id  = localStorage.getItem('worklist_id');

  var planId = localStorage.getItem("worklist_id");
  var teamId = localStorage.getItem("team_id");
  var comment = $('#tConnComment').val();
  var measure = $('#tConnValue').val();

  var baseUrl = localStorage.getItem("baseUrl");



  var item = localStorage.getItem('worklist_item');
  var jsonItem = decodeURI(atob(item));
  var connData = JSON.parse(jsonItem);


  var action = ( par == true ) ? 'complete'  : 'suspend'; //planId;
  // /api/plan_details/suspend/1
  issueObject.id = plan_details_Id; // planId;
  issueObject.remark = comment;
  issueObject.measurement = measure;

  var postURL = baseUrl + '/api/plan_details/' + action;

  console.info('%c[SENDING POST REQUEST (%O)] \nURL:%O\nissueObject: %O', 'color: orange;', postURL, action, issueObject);

  let xhr = new XMLHttpRequest();
  let responseJson = "";
  xhr.open('POST', postURL, true);
  xhr.setRequestHeader('accept', 'application/json');
  xhr.setRequestHeader('Content-Type', 'application/json');
  xhr.onload = function()
  {
    responseJson = xhr.response;
    console.log(responseJson);
    var json = JSON.parse(responseJson);
    console.log(json);

    $('#cover').hide();

    // location.reload();

  };

  xhr.onprogress = function(event) {};
  xhr.onloadend = function (ev) {};
  xhr.onerror = function (ev) {};
  xhr.send(JSON.stringify(issueObject));


    $('#cover').show();



// return;

  let sCount = 0;

  if ( fConnFiles.length == 0 ) {
    console.info('%c[INFO] No images found', 'color: white;');
    localStorage.setItem('redirect_from_conn', true);
    $('#cover').hide();
    $(location).attr('href', 'plan.html');
  }

  $.each(fConnFiles, function (i, item) {

    var uri = encodeURI(localStorage.getItem('baseUrl') + '/api/photos/upload/one');

    var params = {};
    params.plan_id = wl_id;
    params.plan_detail_id = plan_details_Id;
    params.type = 'ELECTRIC_METER';

    var options = new FileUploadOptions();
    options.params = params;

    var ft = new FileTransfer();
    ft.upload(
        item.file,
        uri,
        function(r) {
          console.log(r);
          console.log("Code = " + r.responseCode);
          console.log("Response = " + r.response);
          console.log("Sent = " + r.bytesSent);
          console.log("sendCounter: " + sCount + " (" + fConnFiles.length + ")");
          console.log("==============================");
          sCount ++;
          if ( sCount == fConnFiles.length) {
            // todo redirect ...
            console.log('REDIRECT FROM SUCCESS!!!!!!!!!!!!!!!!!!!!!!!!!!');
            localStorage.setItem('redirect_from_conn', true);
            localStorage.setItem('worklist_frcon_id', wl_id);
            localStorage.setItem('wl_object_id', wlo_id);
            localStorage.setItem('device_type', device_type);
            $(location).attr('href', 'plan.html');
          }

        },
        function(error) {
          sCount ++;
          console.error(error);
          console.log("sendCounter: " + sCount + " (" + fConnFiles.length + ")");
          if ( sCount == fConnFiles.length) {
            // todo redirect ...
            console.log('REDIRECT FROM ERROR!!!!!!!!!!!!!!!!!!!!!!!!!!');
            alert("При отправке снимков произошла ошибка!")
            localStorage.setItem('redirect_from_conn', true);
            localStorage.setItem('worklist_frcon_id', wl_id);
            localStorage.setItem('wl_object_id', wlo_id);
            localStorage.setItem('device_type', device_type);
            $(location).attr('href', 'plan.html');
          }
        },
        options);

  });
}

/**
 *
 * @param par true/false COMPLETED/COMPLETED_WITH_REMARK
 */
function devSendConnIssue(par){
  let jsMsg = '';
  let xhr = new XMLHttpRequest();
  let responseJson = "";

  if ( !testDev ) {
    // LVM 12.04.2021 API DEV BY SK
    // LVM [START] =====================================================================================================
    // todo code here ...
    // LVM [END] =======================================================================================================
  } else {
    // LVM 12.04.2021 API DEV BY RE
    // LVM [START] =====================================================================================================
    xhr.open('POST', 'http://192.168.2.40:3000/rpc/save_em_work_list', true);
    console.log("CONNECTION.JS [POST] >>>>>>>>>>>>> : http://192.168.2.40:3000/rpc/save_em_work_list");
    // LVM [END] =======================================================================================================
  }

  xhr.onreadystatechange = function() {
    if (xhr.readyState === 2) {}
  }
  xhr.setRequestHeader('accept', 'application/json');
  xhr.setRequestHeader('Content-Type', 'application/json');

  xhr.onload = function() {

    responseJson = xhr.response;
    console.log(responseJson);
    var json = JSON.parse(responseJson);
    console.log(json);
    var html = '';

    // LVM 12.04.2021 API DEV BY SK(false)/RE(true)
    // LVM [START] =====================================================================================================
    if ( !testDev ) {
      // todo code here ...
    } else { // LVM RE <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
      // todo code here
      if ( fConnFiles === null || fConnFiles.length == 0 ) {
        console.log('REDIRECT FROM NO FILES!!!!!!!!!!!!!!!!!!!!!!!!!!');
        $('#cover').hide();

        localStorage.setItem('redirect_from_conn', true);
        localStorage.setItem('worklist_frcon_id', wl_id);
        localStorage.setItem('wl_object_id', wlo_id);
        localStorage.setItem('feeder_id_scroll', connId);
        localStorage.setItem('device_type', device_type);
        $(location).attr('href', 'plan.html');
      }

    }
    // LVM [END] =======================================================================================================


  };
  xhr.onprogress = function(event) {};
  xhr.onloadend = function (ev) {};
  xhr.onerror = function (ev) {
    // app_con_log
    console.log('ERROR SENDING REQUEST!');
    console.log(ev);
    console.log('===============================');
    console.log(jsMsg);
    console.log('===============================');

    connNetStResolveFS();

    // todo code here ...
    // todo 1. create JSON file if not exists OR check if possible to parse
    //  JSON if not empty.
    //  If parsed check status isSent. If found delete item from array and update file
    //  After isSent add an array item, with status isSent = false;
    // todo ...
    //  Create check method for not sent data before sending even single request!!!!!!!
    //

  };

  // LVM 12.04.2021 API DEV BY SK(false)/RE(true)
  // LVM [START] =======================================================================================================
  if ( !testDev ) {
    // todo code here ...
  } else {
    let conVal = document.getElementById("tConnValue").value;
    let conCmnt = document.getElementById("tConnComment").value;

    let isComplete = '';
    isComplete = ( par ) ? 'COMPLETED' : 'COMPLETED_WITH_REMARK';
    jsMsg = '{\n' +
        '  "params": {\n' +
        '    "id_plan": "' + wl_id + '",\n' +
        '    "em_works": [\n' +
        '      {\n' +
        '        "id_em": ' + connId + ',\n' +
        '        "feeder": "' + conn_name + '",\n' +
        '        "status": "' + isComplete + '",\n' +
        '        "value": "' + conVal + '",\n' +
        '        "comment": "' + conCmnt + '",\n' +
        '        "device_type": "' + device_type + '"\n' +
        '      }\n' +
        '    ]\n' +
        '  }\n' +
        '}';
    selected_conn_json = jsMsg;
    console.log(jsMsg);
    xhr.send(jsMsg);
  }
  // LVM [END] =========================================================================================================
}

function devSenErrorEmulation(par){
  let conVal = document.getElementById("tConnValue").value;
  let conCmnt = document.getElementById("tConnComment").value;

  let isComplete = '';
  isComplete = ( par ) ? 'COMPLETED' : 'COMPLETED_WITH_REMARK';
  jsMsg = '{\n' +
      '  "params": {\n' +
      '    "id_plan": "' + wl_id + '",\n' +
      '    "em_works": [\n' +
      '      {\n' +
      '        "id_em": ' + connId + ',\n' +
      '        "feeder": "' + conn_name + '",\n' +
      '        "status": "' + isComplete +'",\n' +
      '        "value": "' + conVal +'",\n' +
      '        "comment": "' + conCmnt + '",\n' +
      '        "device_type": "' + device_type + '"\n' +
      '      }\n' +
      '    ]\n' +
      '  }\n' +
      '}';
  selected_conn_json = jsMsg;
  console.log(jsMsg);

  console.log('[ >>> ERROR EMULATOR <<< ] ERROR SENDING REQUEST!');
  connNetStResolveFS();

  localStorage.setItem('redirect_from_conn', false);
  localStorage.setItem('worklist_frcon_id', -1);
  localStorage.setItem('wl_object_id', -1);
  // $(location).attr('href', 'plan.html');
  // todo Not fact app should reach this point due to call backs delay in file reading and closing events
  // todo WARNING WARNING WARNING  Read message above!
  return;

}

/**
 * Метод отправки файла в АПИ на сервер
 * @param fileEntry
 */
function uploadFile(fileEntry) {
  var uri = encodeURI(localStorage.getItem('baseUrl') + '/api/photos/upload/one');
  // uri = 'https://webhook.site/04bc2b67-5a0a-4b9b-8430-58cb427214a1';
  console.log(fileEntry);

  var params = {};
  params.plan_id = wl_id;
  params.em_id = connId;
  params.type = 'ELECTRIC_METER';

  var options = new FileUploadOptions();
  options.params = params;

  var ft = new FileTransfer();
  ft.upload(
      '/sdcard/' + fileEntry.fullPath,
      uri,
      function(r) {
        console.log(r);
        console.log("Code = " + r.responseCode);
        console.log("Response = " + r.response);
        console.log("Sent = " + r.bytesSent);
      },
      function(error) {
        console.log(error);
      },
      options);
  // ft.onProgress((progressEvent) => {
  //   this.events.publish("updater:download", {progress: Math.round(((progressEvent.loaded / progressEvent.total) * 100))});
  // });
}

/**
 * Модель снимка прибора
 * @param id   - счетчик в DOM'е (img_ConCounter)
 * @param file - объект FileEntry(Cordova)
 * @constructor
 */
function ConnectionPhoto(id, filePath){
  this.id = id;
  this.file = filePath;
}

// LVM Формирование JSON файла статуса отправки файла
//
// [START] -------------------------------------------------------------------------------------------------------------
/**
 * Запуск цепочки создания/чтения/редактирования
 * файла состояния отправки подключения
 */
function connNetStResolveFS() {
  window.resolveLocalFileSystemURL(cordova.file.externalApplicationStorageDirectory, createConnNetStFolder, createConnNetStFolderErr);
}

/**
 * Создние папки(папка по умолчанию) [SUCCESS CALLBACK]
 */
function createConnNetStFolder(folder) {
  folder.getDirectory(initialDataFolder, { create: true}, createConnNetStFile, e1);
}

/**
 * Создание папки(папка по умолчанию) [SUCCESS CALLBACK]
 */
function createConnNetStFolderErr(folder) {
  folder.getDirectory(initialDataFolder, { create: false}, createConnNetStFile, e2);
}

/**
 * Создание файла(Если нет файла лоша отправки, или ничего
 * не делать, если файл есть) [SUCCESS CALLBACK]
 * @param folder
 */
function createConnNetStFile(folder) {
  folder.getFile(app_con_log, { create: true }, createConnNetStFileSuccess, createFileError);
}

/**
 * Создание fileWriter'а для лога статуса отправки
 * состояния подключения [SUCCESS CALLBACK]
 * @param file
 */
function createConnNetStFileSuccess(fileEntry) {
  // LVM
  //  1. Зачитать
  //  2. Проверить массив на значения isSent = true
  //  3. Почистить массив, где есть п.2
  //  4. Сформировать новый JSON с учетом записей пп. 1 - 3
  //  5. Записать новые данные
  fileEntry.file(
      function (file) {
        let reader = new FileReader();
        reader.onloadend = function(dev)
        {

          // this -> is a fileReader
          console.log("Successful file read: " + this.result);
          console.log(this.result);

          if ( this.result != '' && this.result !== null ) {
            // todo GENERATE NEW JSON WITH OLD DATA CHECK FOR UNIQUINITY
            //  GET NEWER DATA IF IDs ARE EQUAL ?
            //  ...
            //  ...
            //  ...
            //  LVM FILE FOUND => UPDATING JSON WITH NEW UNSENT DATA !
            let foundData = JSON.parse(this.result);
            let obj = JSON.parse(connSndJsonData);
            $.each(foundData.data, function(i, item) {
              obj['data'].push(item);
            });
            obj['data'].push(JSON.parse(selected_conn_json));
            selected_conn_json = JSON.stringify(obj);
          } else { // LVM FILE NOT FOUND, EMPTY FILE OR NO DATA FOUND => GENERATING NEW !
            let obj = JSON.parse(connSndJsonData);
            obj['data'].push(JSON.parse(selected_conn_json));
            selected_conn_json = JSON.stringify(obj);
          }
          fileEntry.createWriter(fileConnNetFileWriterSuccess, fileConnNetFileWriterError);
        };
        reader.readAsText(file);
      },
      function error(e) {
        console.log("Error read connection log file!");
        console.log(e);
        fileEntry.createWriter(fileConnNetFileWriterSuccess, fileConnNetFileWriterError);
      }
  );

}

/**
 * Запись данных в файл [SUCCESS CALLBACK]
 * @param fileWriter
 */
function fileConnNetFileWriterSuccess(fileWriter) {
  console.log(fileWriter);
  // fileWriter.seek(fileWriter.length); // Required for append file
  console.log('selected_conn_json');
  console.log(selected_conn_json);
  // selected_conn_json = "HUY!"
  // let blob = new Blob(selected_conn_json, 'text/plain');
  var blob = new Blob([selected_conn_json], {type:'text/plain'});
  fileWriter.onwrite = function (e) {
    console.log("WRITE SUCCESS");
    console.log(e);
  }
  fileWriter.onerror =function(e) {
    console.error(e);
  }
  fileWriter.write(blob);
}

function fileConnNetFileWriterError(e){
  console.log(e);
}


// [END] ---------------------------------------------------------------------------------------------------------------


<!-- ERROR CALLBACKS -->

function cameraError(e) {
  $('#cover').hide();
  console.log('ERROR! CAMERA FAILED!');
  console.log(e);
}

function e1(e) {
  $('#cover').hide();
  console.log('ERROR! createFolder()!');
  console.log(e);
}

function e2(e) {
  $('#cover').hide();
  console.log('ERROR! folderExists()!');
  console.log(e);
}

function createFileError(e) {
  $('#cover').hide();
  console.log('ERROR! createFile()!');
  console.log(e);
}

function fileWriterError(e) {
  $('#cover').hide();
  console.log('ERROR! fileWriterError()!');
  console.log(e);
  fConnFiles.slice(-1, 1); // Удаляем последнюю добавленную запись из массива файлов
}

function getBackFromConn(event) {
  $('#cover').hide();
  localStorage.setItem('redirect_from_conn', true);
  localStorage.setItem('worklist_frcon_id', wl_id);
  event.preventDefault();
  window.history.back();
}


// LVM SAMPLES

// JS MODEL ANALOG EXAMPLE
// function ConnectionPhoto(frequency, hours, id, name, num, position, priority, status, unit, workType, action, action_result){
//   this.frequency = frequency;
//   this.hours = hours;
//   this.id = id;
//   this.name = name;
//   this.num = num;
//   this.position = position;
//   this.priority = priority;
//   this.status = status;
//   this.unit = unit;
//   this.workType = workType;
//   this.action_result = action_result;
// }
// Usage example:
// let iObject = new ConnectionPhoto(
//     frequency,
//     hours,
//     id,
//     name,
//     '',
//     positionName,
//     '',
//     '',
//     unit,
//     workType,
//     ''
// );
// itemsArray.push(iObject);

