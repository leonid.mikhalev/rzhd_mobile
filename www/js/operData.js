var appMap;
var latitude, longitude;

/**
 * Метод отслеживающий загрузку ДОМа
 */
$(document).ready(function() {
    appMap = L.map('taskLocation').setView([56.644566, 36.295457], 12);
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(appMap);

    var c1 = L.circle([56.644566, 36.295457], {
        color: 'yellow',
        fillColor: '#f03',
        fillOpacity: 0.5,
        radius: 200
    }).addTo(appMap); //.on("click", myPosClick );
});

document.addEventListener("DOMContentLoaded", () => {});

