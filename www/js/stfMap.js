document.addEventListener("DOMContentLoaded", function(event) {
        setTimeout(function() {
                var id = localStorage.getItem('itemId');
                var iterator = localStorage.getItem('itemIter', iterator);
                //console.log('Within STAFFMAP renderMenu(iterator = ' + iterator + ', id = ' + id + ');');

                if ( id < 0 ) return;

                var html =
                    '<li class="nav-item">\n' +
                    '<a class="nav-link" href="taskList.html">Список задач</a>\n' +
                    '</li>\n' +
                    '<li class="nav-item">\n' +
                    '<a class="nav-link" href="staffMap.html">Местонахождение</a>\n' +
                    '</li>\n' +
                    '<li class="nav-item">\n' +
                    '<a class="nav-link" onclick="getPic(' + id + ',' + iterator + ')">Сделать снимок</a>\n' + //  id = ' + id + ', iter = ' + iterator + '
                    '</li>\n' +
                    '<li class="nav-item">\n' +
                    '<a class="nav-link" onclick="getOrder(' + id + ',' + iterator + ')">Регламент</a>\n' +
                    '</li>\n' +
                    '<li class="nav-item">\n' +
                    '<a class="nav-link" onclick="getAttachedDocs(' + id + ',' + iterator + ')">Документы</a>\n' +
                    '</li>\n' +
                    '<li class="nav-item">\n' +
                    '<a class="nav-link" onclick="getOperData(' + id + ',' + iterator + ')">Оперативная информация</a>\n' +
                    '</li>\n' +
                    '<li class="nav-item">\n' +
                    '<a class="nav-link" onclick="partsOrder(' + id + ',' + iterator + ')">Заказ оборудования</a>\n' +
                    '</li>\n' +
                    '<li class="nav-item">\n' +
                    '<a class="nav-link" onclick="act12(' + id + ',' + iterator + ')">Акт осмотра</a>\n' +
                    '</li>\n' +
                    '<li class="nav-item">\n' +
                    '<a class="nav-link" onclick="act13(' + id + ',' + iterator + ')">Акт не допуска</a>\n' +
                    '</li>\n' +
                    '<li class="nav-item">\n' +
                    '<a class="nav-link" onclick="actChange(' + id + ',' + iterator + ')">Акт замены</a>\n' +
                    '</li>\n' +
                    '<li class="nav-item">\n' +
                    '<a class="nav-link" onclick="actCorrupt(' + id + ',' + iterator + ')">Акт дефектации</a>\n' +
                    '</li>';
                try {
                        // console.log(html);
                        // console.log($('#menuList'));
                        $('#menuList').empty();
                        $('#menuList').append(html);
                } catch (e) {
                        console.log(e.stackTrace);
                        console.log(e);
                }
                // console.log(html);
                //console.log('Staff map menu render completed ...');
        }, 10);
});

function getOrder(id, iter) { $(location).attr('href', 'order.html'); }
function getAttachedDocs(id, iter) { $(location).attr('href', 'docList.html'); }
function getOperData(id, iter) { location.replace("operData.html"); }
function partsOrder(id, iter) { $(location).attr('href', 'partsOrder.html'); }

function act12(id, iterator) { $(location).attr('href', 'act12.html'); }
function act13(id, iterator) { $(location).attr('href', 'act13.html'); }
function actChange(id, iterator) { $(location).attr('href', 'actChange.html'); }
function actCorrupt(id, iterator) { $(location).attr('href', 'actCorrupt.html'); }

// todo Code to send form data to server and to keep result
//      in json file in case no cellular network available.
function sendAct12() { sendOrder(); }
function sendAct13() { sendOrder(); }
function sendActChange() { sendOrder(); }
function sendActCorrupt() { sendOrder(); }

function sendOrder(){ $(location).attr('href', 'taskList.html'); }