/**
 * Глобальные переменные
 */
var imgUri;                                // BASE64 блоб картинки
var id_img;                                // идентификатор для картинки
var iterator_img;                          // итератор картинки, номер задачи, к которой она относится
var taskId = undefined;                    // идентификатор задачи
var taskIter = undefined;                  // итератор задачи, номер задачи в списке
var taskStatus;                            // статус задачи
var taskListLength = -1;                   // Длина массива списка задач
var itemsArray = [];                       // Массив объектов списка задач
var baseUrl;                               // = 'http://192.168.2.40:8080';

/**
 * Метод отслеживающий загрузку ДОМа
 */
$(document).ready(function() {
    // todo code here ...
});

/**
 * Метод отслеживающий полную загрузку ДОМа
 */
document.addEventListener("DOMContentLoaded", () => {
    taskId = localStorage.getItem('itemId');
    taskIter = localStorage.getItem('itemIter');
    this.baseUrl = localStorage.getItem('baseUrl');
    getTaskList();

});

/**
 * Метод получения списка задач
 */
function getTaskList() {
    var worklist_id = localStorage.getItem('worklist_id');
    var wl_object_id = localStorage.getItem('wl_object_id');
    var conn_id = localStorage.getItem('conn_id');

    if ( worklist_id < 0 ) return;

    var xhr = new XMLHttpRequest();
    var responseJson = "";

    xhr.open('GET', this.baseUrl + '/api/owi?object_id=' + wl_object_id + '&em_id=' + conn_id, true);

    console.log("TASKLIST.JS >>>>>>>>>>>>>>>> : " + this.baseUrl + '/api/owi?object_id=' + wl_object_id + '&em_id=' + conn_id);

    xhr.onreadystatechange = function() {
        if (xhr.readyState === 2) {}
    }
    xhr.setRequestHeader('Access-Control-Allow-Origin', '*');
    xhr.setRequestHeader('accept', 'application/json');
    xhr.setRequestHeader('Content-Type', 'application/json');

    xhr.onload = function() {
        responseJson = xhr.response;
        var json = JSON.parse(responseJson);
        console.log(json);

        $.each(json, function(i, item) {
            console.log(item);
            var iObject = new itemObject(
                item.frequencyName,
                item.hours,
                item.id,
                item.name,
                '',
                item.positionName,
                '',
                '',
                item.unit,
                item.workType,
                ''
            );
            itemsArray.push(iObject);
            createItemsAccordion(iObject, i);
        });
        taskListLength = itemsArray.length;
    };
    xhr.onprogress = function(event) {};
    xhr.onloadend = function (ev) {}
    xhr.onerror = function (ev) {}
    xhr.send();
}

/**
 * Модель объекта сущности списка задач
 * @param frequency
 * @param hours
 * @param id
 * @param name
 * @param num
 * @param position
 * @param priority
 * @param status
 * @param unit
 * @param workType
 * @param action
 * @param action_result
 */
function itemObject(frequency, hours, id, name, num, position, priority, status, unit, workType, action, action_result){
    this.frequency = frequency;
    this.hours = hours;
    this.id = id;
    this.name = name;
    this.num = num;
    this.position = position;
    this.priority = priority;
    this.status = status;
    this.unit = unit;
    this.workType = workType;
    this.action_result = action_result;
}

/**
 * Метод создания списка из запроса к АПИ
 * @param itemObject - Модель списка
 * @param iterator   - Порядковый номер в списке
 */
function createItemsAccordion(itemObject, iterator) {

    console.log("itemObj = " + itemObject + ", i = " + iterator);

    var isCollapse = 'collapse show';
    var collapsedHdr = 'collapsed';
    var isDisabled = '';
    var badgeClass = 'badge-primary';
    var badgeValue = 'Активно';
    if ( iterator == 0 ) collapsedHdr = 'collapsed'; else isCollapse = 'collapse';
    if ( iterator == 0 ) isCollapse = 'collapse show'; else isCollapse = 'collapse';

    if ( iterator > 0 ) {
        // isDisabled = 'disabled'; // fixme LVM 12.02.2021 Убрал, чтобы иметь возможность раскрывать все задачи
        this.taskStatus = 'scheduled'
    } else {
        this.taskStatus = 'active'
    }
    if (this.taskStatus == 'active') {
        this.taskIter = iterator;
        this.taskId = itemObject.id;
    }

    if ( taskStatus == 'scheduled' ) {
        badgeClass = 'badge-info';
        badgeValue = 'Запланировано';
    } else if ( taskStatus == 'done' ) {
        badgeClass = 'badge-success';
        badgeValue = 'Выполнено';
    } else if ( taskStatus == 'canceled' ) {
        badgeClass = 'badge-danger';
        badgeValue = 'Отменено';
    }
    // else {
    //     badgeClass = 'badge-primary';
    //     badgeValue = 'Активно';
    // }

    var html = '    <div class="card">\n' +
        '             <div class="card-header" href="#collapse' + iterator + '" data-toggle="collapse" data-target="#collapse' + iterator + '">\n' + //  is-disabled="true">\n' +
        '               <a class="card-link">\n' +
        (iterator + 1) + '. <span class="badge ' + badgeClass + '" id="badgeTask' + iterator+ '" >' + badgeValue + '</span>&nbsp<br><b>' + itemObject.workType + '.<br>' + itemObject.unit + '.<br></b>' + itemObject.name + '\n' +
        '               </a>\n' +
        '             </div>\n' +
        '             <div id="collapse' + iterator + '" class="' + isCollapse + '" data-parent="#itemsAccordion">\n' +
        '               <div class="card-body">' +
        '                 <ul class="list-group list-group-flush">\n' +
        '                   <li class="list-group-item">' +
        '                     <div class="form-group" id="commentContainer">\n' +
        '                       <label for="comment">Пояснение&#x2FПрикрепить снимок </label>\n' +
        '                         <div class="row">\n' +
        '                           <div class="input-group col-12">\n' +
        '                             <input id="comment' + iterator + '" class="form-control" type="text" name="comment" placeholder="">\n' +
        '                             <div class="input-group-append">\n' +
        // todo LVM 01.03.2121 uncomment line below to take picture and remove one development line after
        // '                               <span class="input-group-text" onclick="getPic(' + itemObject.id + ',' + iterator + ')" style="border-radius: 5px;"><i class="fas fa-camera"></i></span>\n' +
        '                               <span class="input-group-text" onclick="getFileHandler()" style="border-radius: 5px;"><i class="fas fa-camera"></i></span>\n' +
        '                             </div>\n' +
        '                              <div class="invalid-feedback hidden" id="isEmptyComment' + iterator + '">Необходимо добавить пояснение!</div>\n' +
        '                           </div>\n' +
        '                         </div>\n' +
        '                     </div>    \n' +
        '                     <div class="form-group" id="indicContainer">\n' +
        '                       <label for="indication">Показания</label>\n' +
        '                         <div class="row">\n' +
        '                           <div class="col-12">\n' +
        '                             <input id="indication' + iterator + '" class="form-control" type="text" name="indication" placeholder="">\n' +
        '                               <div class="invalid-feedback hidden" id="isEmptyIndication' + iterator + '">Необходимо добавить показания!</div>\n' +
        '                           </div>\n' +
        '                         </div>\n' +
        '                     </div>    \n' +
        '                   </li>' +
        '                   <li class="list-group-item" id="picContainer' + iterator + '" hidden>\n' +
        '                     <img id="objectPic' + iterator + '" style="width: 100%; height: auto;" />\n' +
        '                   </li>\n' +
        '                   <li class="list-group-item">' +
        '                     <div class="d-flex justify-content-start flex-wrap">' +
        '                       <button type="button" class="btn btn-outline-success ' + isDisabled + '" onclick="setCredentials(' + itemObject.id + ',' + iterator + ',' + true +')" id="bCloseTask' + iterator + '">Завершить</button>' +
        '                       <button type="button" class="btn btn-outline-danger ' + isDisabled + '" onclick="setCredentials(' + itemObject.id + ',' + iterator + ',' + false +')" style="margin-left: 10px;" id="bCancelTask' + iterator + '">Отклонить</button>' +
        '                     </div>' +
        '                   </li>\n' +
        '                 </ul>\n' +
        '               </div>\n' +
        '             </div>\n' +
        '           </div>';

    $('#itemsAccordion').append(html);

    if ( isCollapse == 'collapse show' ) {

        localStorage.setItem('itemId', itemObject.id);
        localStorage.setItem('itemIter', iterator);

        renderMenu(iterator, itemObject.id)
    }

}

function renderMenu(iterator, id){
    localStorage.setItem('itemId', id);
    localStorage.setItem('itemIter', iterator);
    console.log('Within taskListJS renderMenu(' + iterator + ', ' + id + ');');

    if ( id < 0 ) return;

    var html =
    '<li class="nav-item">\n' +
        '<a class="nav-link" href="taskList.html">Список задач</a>\n' +
    '</li>\n' +
    '<li class="nav-item">\n' +
        '<a class="nav-link" href="staffMap.html">Местонахождение</a>\n' +
    '</li>\n' +
    '<li class="nav-item">\n' +
        '<a class="nav-link" onclick="getPic(' + id + ',' + iterator + ')">Сделать снимок</a>\n' + //  id = ' + id + ', iter = ' + iterator + '
    '</li>\n' +
    '<li class="nav-item">\n' +
        '<a class="nav-link" onclick="getOrder(' + id + ',' + iterator + ')">Регламент</a>\n' +
    '</li>\n' +
    '<li class="nav-item">\n' +
        '<a class="nav-link" onclick="getAttachedDocs(' + id + ',' + iterator + ')">Документы</a>\n' +
    '</li>\n' +
    '<li class="nav-item">\n' +
        '<a class="nav-link" onclick="getOperData(' + id + ',' + iterator + ')">Оперативная информация</a>\n' +
    '</li>\n' +
    '<li class="nav-item">\n' +
    '<a class="nav-link" onclick="partsOrder(' + id + ',' + iterator + ')">Заказ оборудования</a>\n' +
    '</li>\n' +
    '<li class="nav-item">\n' +
    '<a class="nav-link" onclick="act12(' + id + ',' + iterator + ')">Акт осмотра</a>\n' +
    '</li>\n' +
    '<li class="nav-item">\n' +
    '<a class="nav-link" onclick="act13(' + id + ',' + iterator + ')">Акт не допуска</a>\n' +
    '</li>\n' +
    '<li class="nav-item">\n' +
    '<a class="nav-link" onclick="actChange(' + id + ',' + iterator + ')">Акт замены</a>\n' +
    '</li>\n' +
    '<li class="nav-item">\n' +
    '<a class="nav-link" onclick="actCorrupt(' + id + ',' + iterator + ')">Акт дефектации</a>\n' +
    '</li>';
    $('#menuList').empty();
    $('#menuList').append(html);
}

function getOrder(id, iter) { $(location).attr('href', 'order.html'); }
function getAttachedDocs(id, iter) { $(location).attr('href', 'docList.html'); }
function getOperData(id, iter) { location.replace("operData.html"); }
function partsOrder(id, iter) { $(location).attr('href', 'partsOrder.html'); }
function act12(id, iterator) { $(location).attr('href', 'act12.html'); }
function act13(id, iterator) { $(location).attr('href', 'act13.html'); }
function actChange(id, iterator) { $(location).attr('href', 'actChange.html'); }
function actCorrupt(id, iterator) { $(location).attr('href', 'actCorrupt.html'); }


/**
 * Метод сохранения глобальных переменных id задачи и i - порядкового номера в списке
 * @param id
 * @param i
 * @param bModal - true - Завершить задачу, false - Отклонить задачу
 */
function setCredentials(id, i, bModal) {
    this.taskId = id;
    this.taskIter = i;

    var isNotAllowed = document.getElementById('bCloseTask' + taskIter).classList.contains('disabled');

    if ( isNotAllowed ) return;

    if ( bModal ) {
        $('#modalConfirm').modal('toggle');
    } else {
        $('#modalDismiss').modal('toggle');
    }

}

/**
 * Метод закрытия задачи
 */
function closeTask() {

    var b = '#makePic' + taskIter;
    //if ($(b).hasClass('disabled')) return;


    var strId = 'comment' + this.taskIter;
    var res = document.getElementById(strId).value;
    var valid = false;
    // todo Проверка заполненного поля "Пояснения"
    // if ( res == '' ) {
    //     $('#isEmptyComment' + this.taskIter).removeClass("hidden");
    //     $('#comment' + this.taskIter).addClass("is-invalid");
    // } else {
    //     $('#isEmptyComment' + this.taskIter).addClass("hidden");
    //     $('#comment' + this.taskIter).removeClass("is-invalid");
    //     valid = true;
    // }
    // if (valid) {
    if (true) {

        invalidateStatus(taskId, taskIter, false);
        // todo send image to server method here ...
        this.taskId = itemsArray[taskIter].id;
        renderMenu(taskIter, taskId);

        return;
    }

}

/**
 * Метод отмены задачи
 */
function cancelTask() {
    var b = '#makePic' + taskIter;
    if ($(b).hasClass('disabled')) return;

    var strId = 'comment' + this.taskIter;
    var res = document.getElementById(strId).value;
    var valid = false;

    // todo Проверка заполненного поля "Пояснения"
    // if ( res == '' ) {
    //     $('#isEmptyComment' + this.taskIter).removeClass("hidden");
    //     $('#comment' + this.taskIter).addClass("is-invalid");
    // } else {
    //     $('#isEmptyComment' + this.taskIter).addClass("hidden");
    //     $('#comment' + this.taskIter).removeClass("is-invalid");
    //     valid = true;
    // }
    //if (valid) {
    if (true) {
        invalidateStatus(taskId, taskIter, true);
        // todo send image to server method here ...
        this.taskId = itemsArray[taskIter].id;
        renderMenu(taskIter, taskId);

        return;
    }
}

function invalidateStatus(taskId, taskIter, isDismissed) {
    var item = document.getElementById('badgeTask' + taskIter);
    if (!isDismissed) {
        item.textContent = "Выполнено";
        item.classList.remove("badge-primary");
        item.classList.remove("badge-info");
        item.classList.add("badge-success");
    } else {
        item.textContent = "Отклонено";
        item.classList.remove("badge-primary");
        item.classList.add("badge-danger");
    }

    // document.getElementById('makePic' + taskIter).classList.add("disabled");
    document.getElementById('bCloseTask' + taskIter).classList.add("disabled");
    document.getElementById('bCancelTask' + taskIter).classList.add("disabled");
    document.getElementById('collapse' + taskIter).classList.remove("show");
    document.getElementById('collapse' + taskIter).classList.add("collapse");

    if (!isLast() && taskIter > -1) {
        // todo ... логика работы с АПИ

        // Работа с перерисовкой интерфейса
        taskIter++;
        this.taskIter = taskIter;
        taskId = itemsArray[taskIter].id;
        var bClose = document.getElementById('bCloseTask' + taskIter);
        var bCancel = document.getElementById('bCancelTask' + taskIter);
        var stsBadge = document.getElementById('badgeTask' + taskIter);
        if (bClose.classList.contains('disabled')) bClose.classList.remove("disabled");
        if (bCancel.classList.contains('disabled')) bCancel.classList.remove("disabled");
        document.getElementById('collapse' + taskIter).classList.remove("collapse");
        document.getElementById('collapse' + taskIter).classList.add("show");
    }
}

function isLast() {
    if ( this.taskIter < 0 ) return false;

    if ( taskListLength == this.taskIter + 1 ) return true;
    else return false;
}

/**
 * Инициализация камеры
 */
function getPic(id, i){

    this.id_img = id;
    this.iterator_img = i;
    var options = {
        quality: 100,
        // destinationType: Camera.DestinationType.DATA_URL,
        destinationType: Camera.DestinationType.FILE_URI,
        // destinationType: Camera.DestinationType.NATIVE_URI,
        sourceType: Camera.PictureSourceType.CAMERA,
        mediaType: Camera.MediaType.PICTURE,
        encodingType: Camera.EncodingType.PNG,
        cameraDirection: Camera.Direction.BACK,
        correctOrientation: true,
        targetWidth: 1440,
        targetHeight: 2037
    };

    window.screen.orientation.lock('portrait');

    navigator.camera.getPicture(cameraSuccess, cameraError, options);
}

/** window.resolveLocalFileSystemURL(imgUri,
 * Callback камеры = file:///data/user/0/com.system.location/cache/1606721275818.jpg
 */
function cameraSuccess(imgURI) {
    savePhoto(imgURI);
}

/**
 *  Callback камеры
 */
function cameraError(msg) { console.log(msg); }

/**
 * Save image to specific folder
 * @param imgURI
 */
function savePhoto(imgLink) {

    var currentdate = new Date();
    var fileNameVar = currentdate.getDate() + "_"
        + (currentdate.getMonth()+1)  + "_"
        + currentdate.getFullYear() + "T"
        + currentdate.getHours() + "_"
        + currentdate.getMinutes() + "_"
        + currentdate.getSeconds()+'.png';

    window.resolveLocalFileSystemURL(cordova.file.cacheDirectory, function success(dirEntry) {

        // Создаем файл, куда будем сохранять файл
        dirEntry.getFile(fileNameVar, { create: true, exclusive: false }, function (fileEntry) {

            // дотягиваемся до файла снимка
            resolveLocalFileSystemURL(imgLink, function(fentry) {

                fentry.file(function (file) {
                    var reader = new FileReader();

                    reader.onloadend = function() {
                        var base64 = btoa(
                            new Uint8Array(this.result)
                                .reduce((data, byte) => data + String.fromCharCode(byte), '')
                        );

                        writeFile(fileEntry, base64, fileNameVar, fentry);
                    };

                    reader.readAsArrayBuffer(file);

                }, fsError);
            });

            return;

        }, fsError);

    }, fsError);
}

function writeFile(fileEntry, dataObj, fileNameVar, fentry) {
    // Create a FileWriter object for our FileEntry (log.txt).
    fileEntry.createWriter(function (fileWriter) {

        // console.log(fileEntry);

        fileWriter.onwriteend = function() {

            // console.log("Successful file write...");
            readFile(fileEntry);
            var uri = encodeURI(localStorage.getItem('baseUrl') + '/api/photos/upload/one');
            // console.log('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> upload URI : ' + uri);
            // console.log(fileNameVar);
            // console.log(fileEntry);

            // uri = 'https://webhook.site/77d71de5-bb41-4987-b0a7-31f977850e39';

            var options = new FileUploadOptions();
            options.fileKey="file";
            options.fileName='/data/data/com.system.location/cache/' + fileNameVar
            options.mimeType="image/png";
            options.httpMethod='POST';
            options.accept='application/json';
            options.chunkedMode=false;

            var params = {};
            params.createdAt = fileNameVar.replace('.png','');
            var ta = params.createdAt.split('T')
            var d1 = ta[0].split('_'); //dd mm yyyy
            var dfinal = d1[2] + '-' + d1[1] + '-' + d1[0];

            var t1 = ta[1].split('_'); //hh mm ss
            var h1 = parseInt([t1[0]]) < 10 ? '0' + t1[0] : t1[0];
            var m1 = parseInt([t1[1]]) < 10 ? '0' + t1[1] : t1[1];
            var s1 = parseInt([t1[2]]) < 10 ? '0' + t1[2] : t1[2];
            var tfinal = h1 + ':' + m1 + ':' + s1 + '.000+03:00';
            var dtfinal = dfinal + 'T' + tfinal;

            params.createdAt = dtfinal;

            // console.log(params.createdAt);

            params.planId = localStorage.getItem('worklist_id');
            params.workListId = localStorage.getItem('worklist_id');

            options.params = params;

            var ft = new FileTransfer();
            ft.upload('/data/data/com.system.location/cache/' + fileNameVar, uri, function(r){
                // console.log(r);
                // console.log("Code = " + r.responseCode);
                // console.log("Response = " + r.response);
                // console.log("Sent = " + r.bytesSent);
                // fentry.remove(function (file) { console.log(file.name + ' has been removed.'); },
                //     function(error) { console.log(error); },
                //     function(){ console.log('File does not exist'); });
            }, function(error){ console.log(error); }, options);

        };

        fileWriter.onerror = function (e) {
            console.log("Failed file write: " + e.toString());
        };

        dataObj = dataObj.replace("data:image/jpeg;base64,", '');
        var blob = b64toBlob(dataObj, 'image/png');
        fileWriter.write(blob);
    });
}

function readFile(fileEntry) {

    fileEntry.file(function (file) {
        var reader = new FileReader();

        reader.onloadend = function() {

            // console.log("Successful file read: " + this.result);
            var base64 = btoa(
                new Uint8Array(this.result)
                    .reduce((data, byte) => data + String.fromCharCode(byte), '')
            );

            $('#objectPic' + iterator_img).empty();
            $('#objectPic' + iterator_img).attr("src", 'data:image/jpeg;base64,' + base64);
            $('#picContainer' + iterator_img).attr('hidden', false);
        };

        reader.readAsArrayBuffer(file);
    }, fsError);
}

function b64toBlob(b64Data, contentType, sliceSize) {
    contentType = contentType || '';
    sliceSize = sliceSize || 512;

    var byteCharacters = atob(b64Data);
    var byteArrays = [];

    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        var slice = byteCharacters.slice(offset, offset + sliceSize);

        var byteNumbers = new Array(slice.length);
        for (var i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
        }

        var byteArray = new Uint8Array(byteNumbers);

        byteArrays.push(byteArray);
    }

    var blob = new Blob(byteArrays, {type: contentType});
    return blob;
}

function fsError(error) { console.log(error); }


