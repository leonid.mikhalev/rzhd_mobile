var baseUrl = '';
var team_Id = -1;

document.addEventListener("DOMContentLoaded", function(event) {
  let par1 = localStorage.getItem('team_status');
  let par2 = localStorage.getItem('team_plan_terms');
  let par3 = localStorage.getItem('baseUrl');
  team_Id = localStorage.getItem('team_id');

  localStorage.clear();
  setTeamStatus(par1);
  setTeamTerms(par2);

  switch ( par3 ) {
    case 'http://m2.smk-systems.com:8080': getBaseUrl('DEMO',0); break;
    case 'http://192.168.2.40:8080': getBaseUrl('TEST',0); break;
    case 'http://192.168.2.187:8080': getBaseUrl('DEVELOP',0); break;
  }

});

/**
 * Срок, сегодня, на месяц, на месяц(текущий) на неделю, на неделю(текщую)
 * @param par1
 */
function setTeamTerms(par1) {
  switch (par1) {
    case 'all':
      document.getElementById("bSelectTerms").value = 'Все';
      document.getElementById("bSelectTerms").innerText = 'Все';
      break;
    case 'today':
      document.getElementById("bSelectTerms").value = 'На сегодня';
      document.getElementById("bSelectTerms").innerText = 'На сегодня';
      break;
    case 'on_week':
      document.getElementById("bSelectTerms").value = 'На неделю начиная с сегодня';
      document.getElementById("bSelectTerms").innerText = 'На неделю начиная с сегодня';
      break;
    case 'all_week':
      document.getElementById("bSelectTerms").value = 'На неделю начиная с начала недели';
      document.getElementById("bSelectTerms").innerText = 'На неделю начиная с начала недели';
      break;
    case 'on_month':
      document.getElementById("bSelectTerms").value = 'На текущий месяц начиная с сегодня';
      document.getElementById("bSelectTerms").innerText = 'На текущий месяц начиная с сегодня';
      break;
    case 'all_month':
      document.getElementById("bSelectTerms").value = 'На весь текущий месяц';
      document.getElementById("bSelectTerms").innerText = 'На весь текущий месяц';
      break;
    default:
      par1 = 'all';
      document.getElementById("bSelectTerms").value = 'Все';
      document.getElementById("bSelectTerms").innerText = 'Все';
      break;
  }

  localStorage.setItem('team_plan_terms', par1);

}

/**
 * Метод установки выбранного статуса плана бригады
 * @param par1
 * IN_APPROVE | SCHEDULED | ACCEPTED | AGREED | IN_PROGRESS | COMPLETED | SUSPENDED | UNKNOWN
 */
function setTeamStatus(par1) {
  switch (par1) {
    case 'ALL':
      document.getElementById("bPlanStatus").value = 'Все';
      document.getElementById("bPlanStatus").innerText = 'Все';
      break;
    case 'SCHEDULED':
      document.getElementById("bPlanStatus").value = 'Запланировано';
      document.getElementById("bPlanStatus").innerText = 'Запланировано';
      break;
    case 'COMPLETED':
      document.getElementById("bPlanStatus").value = 'Завершено';
      document.getElementById("bPlanStatus").innerText = 'Завершено';
      break;
    case 'COMPLETED_WITH_REMARK':
      document.getElementById("bPlanStatus").value = 'Завершено с замечаниями';
      document.getElementById("bPlanStatus").innerText = 'Завершено с замечаниями';
      break;
    case 'SUSPENDED':
      document.getElementById("bPlanStatus").value = 'Приостановлено';
      document.getElementById("bPlanStatus").innerText = 'Приостановлено';
      break;
    case 'IN_APPROVE':
      document.getElementById("bPlanStatus").value = 'На утверждении';
      document.getElementById("bPlanStatus").innerText = 'На утверждении';
      break;
    case 'ACCEPTED':
      document.getElementById("bPlanStatus").value = 'Принято';
      document.getElementById("bPlanStatus").innerText = 'Принято';
      break;
    case 'AGREED':
      document.getElementById("bPlanStatus").value = 'Согласовано';
      document.getElementById("bPlanStatus").innerText = 'Согласовано';
      break;
    case 'IN_PROGRESS':
      document.getElementById("bPlanStatus").value = 'В работе';
      document.getElementById("bPlanStatus").innerText = 'В работе';
      break;
    case 'UNKNOWN':
      document.getElementById("bPlanStatus").value = 'Не определено';
      document.getElementById("bPlanStatus").innerText = 'Не определено';
      break;

  }
  localStorage.setItem('team_status', par1);
}

/**
 * Получение адреса сервера
 * @param par
 */
function getBaseUrl(par1, par2) {
  // console.log(par1);
  if ( par1 === undefined || par1 == '' ) return;
  switch (par1) {
    case 'DEMO':
      localStorage.setItem('baseUrl', 'http://m2.smk-systems.com:8080');
      document.getElementById("bSelectSrv").value = 'Демонстрационный';
      document.getElementById("bSelectSrv").innerText = 'Демо сервер';
      break;
    case 'TEST':
      localStorage.setItem('baseUrl', 'http://192.168.2.40:8080');
      document.getElementById("bSelectSrv").value = 'Тестовый';
      document.getElementById("bSelectSrv").innerText = 'Тестовый сервер';
      break;
    case 'DEVELOP':
      localStorage.setItem('baseUrl', 'http://192.168.2.187:8080');
      document.getElementById("bSelectSrv").value = 'Разработка';
      document.getElementById("bSelectSrv").innerText = 'Сервер разработки';
      break;
  }

  switch (par2) {
    case 0:
      $('#bSelectTeam').show();
      $('#bLogin').show();
      break;
    case 1:
      $('#bLogin').show();
      break;
  }

  this.baseUrl = localStorage.getItem('baseUrl');
  getTeams();
}
// -------------------------------------------------------------------------

function redirectToPlan(){
  let par1 = localStorage.getItem('baseUrl');
  let par2 = localStorage.getItem('team_id');
  let par3 = localStorage.getItem('team_status');
  let par4 = localStorage.getItem('team_plan_terms');
  localStorage.setItem('baseUrl', par1);
  localStorage.setItem('team_id', par2);
  localStorage.setItem('team_status', par3);
  localStorage.setItem('team_plan_terms', par4);
  $(location).attr('href', 'plan.html');
}

/**
 * Выбор бригады
 */
function setTeam(par1, teamName) {
  this.team_Id = par1;
  localStorage.setItem('team_id', this.team_Id);
  $('#bLogin').show();

  document.getElementById("bSelectTeam").value = teamName;
  document.getElementById("bSelectTeam").innerText = teamName;
}

/**
 | UNKNOWN	       |    Неизвестно	  |    Сведений о состоянии бригады нет.                          |
 | READY	           |    Ожидание	  |    Бригада готова к работе.                                   |
 | MOVE	           |    В пути	      |    Движение на транспортном средстве.                         |
 | ARRIVED	       |    На объекте	  |    Бригада прибыла на объект.                                 |
 | IN_PROGRESS      |    В работе	  |    Допуск получен, бригада приступила к работе на объекте.    |
 | NOT_AVAILABLE    |    Недоступна	  |    Бригада недоступна                                         |
 */
function getTeams(){

  if ( this.baseUrl == '' ) return;

  console.log(this.baseUrl + '/api/teams');

  var xhr = new XMLHttpRequest();
  var responseJson = "";
  xhr.open('GET', this.baseUrl + '/api/teams', true);
  xhr.setRequestHeader('accept', 'application/json');
  xhr.setRequestHeader('Content-Type', 'application/json');
  xhr.onload = function() {
    let dev_team_name = '';
    responseJson = xhr.response;
    var json = JSON.parse(responseJson);
    var badge = 'badge-info';
    var html = '';
    $('#tmMenu').empty();
    if ( json.rows.length > 0 ) {
      $('#bSelectTeam').prop('disabled', false);
      $('#bPlanStatus').prop('disabled', false);
      $('#bSelectTerms').prop('disabled', false);
    }

    $.each(json.rows, function(i, item) {
      // console.log(item);
      switch (item.status) {
        case 'READY':         badge = 'badge badge-info'; break;
        case 'MOVE':          badge = 'badge badge-primary'; break;
        case 'ARRIVED':       badge = 'badge badge-success'; break;
        case 'IN_PROGRESS':   badge = 'badge badge-info'; break;
        case 'UNKNOWN':       badge = 'badge badge-warning'; break;
        case 'NOT_AVAILABLE': badge = 'badge badge-danger'; break;
      }

      var divider = '';
      if ( i < json.rows.length - 1 ) {
        divider = '    <div class="dropdown-divider" style="border-color: rgba(255, 255, 255, 0.2); margin: 15px 20px 15px 20px;"></div>\n'
      } else {
        divider = '';
      }

      if ( item.id == team_Id && team_Id > -1) {
        dev_team_name = item.name;
      }

      html += '<div onclick="setTeam(' + item.id + ', \'' + item.name + '\')">\n' +
          '  <li>\n' +
          '    <a href="#" style="margin-left: 10px; padding-right: 10px; color: white; position: initial;">' + item.name + '</a>\n' +
          '    <badge class="' + badge + '">' + item.statusLocalized + '</badge>\n' +
          divider +
          '</li>\n' +
          '</div>\n';

    });
    $('#tmMenu').append(html);

    if ( team_Id > -1 && dev_team_name != '' ) { setTeam( team_Id, dev_team_name); }

  };
  xhr.onprogress = function(event) {};
  xhr.onloadend = function (ev) {};
  xhr.onerror = function (ev) {};
  xhr.send();
}

function hideElements(par2) {
  switch (par2) {
    case 0:
      $('#bSelectTeam').hide();
      $('#bLogin').hide();
      break;
    case 1:
      $('#bLogin').hide();
      break;
  }
}
