/**
 * Класс построения событий для календаря
 */

import {Spinner} from './spinner.js';
import {Calendar} from './calendar.js';

const cal = Calendar('calendar');
const spr = Spinner('calendar');

// let baseUrl;
// let team_id2 = -1;

document.addEventListener("DOMContentLoaded", function (event){
    // console.log('>>>>>>>>>>>>> INDEX.JS DOMLOADED 000000000000000000000000000000000');
    // spr.renderSpinner().delay(0);
});

var mckData = '';
var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        this.receivedEvent('deviceready');
    },

    // Update DOM on a Received Event
    receivedEvent: function(id) {}
};

// $(document).ready(function() {
//     // console.log('>>>>>>>>>>>>> INDEX.JS DOC READY 111111111111111111111111111111111111111');
//     // console.log('INDEX.JS team_id = ' + this.team_id);
//     // this.team_id2 = localStorage.getItem('team_id')
//     this.baseUrl = localStorage.getItem('baseUrl');
//
//     var xhr = new XMLHttpRequest();
//     var responseJson = "";
//
//     xhr.open('GET', this.baseUrl + '/api/plans?team_id=' + this.team_id, true);
//     xhr.onreadystatechange = function() {
//         if (xhr.readyState === 2) {}
//     }
//     xhr.setRequestHeader('Access-Control-Allow-Origin', '*');
//     xhr.setRequestHeader('accept', 'application/json');
//     xhr.setRequestHeader('Content-Type', 'application/json');
//
//     xhr.onload = function() {
//         responseJson = xhr.response;
//         var json = JSON.parse(responseJson);
//         console.log(json);
//         mckData = '[';
//         $.each(json.rows, function(i, item) {
//             // console.log('i = ' + i + ', item = ' + item);
//             var dtStr = item.dplan.substring(0,10);
//             var dtParts = dtStr.split('-');
//             var dtStart = dtParts[2] + '.' + dtParts[1] + '.' +dtParts[0];
//             var id = item.id;
//             var status = item.status;
//             var wrkType = item.workType;
//             var badge = 'badge-info';
//             // console.log('i = ' + i + ', id = ' + id + ', status = ' + status + ', dtStart = ' + dtStart + ', wrkType = ' + wrkType);
//             var colr = 'bg-cyan-alt';
//             var sWkTp = wrkType.replace(' ', '');
//             switch ( sWkTp ) {
//                 case 'РТО':
//                     colr = 'bg-cyan-alt';
//                     break;
//                 case 'OTO':
//                     colr = 'bg-orange-alt';
//                     break;
//             }
//
//             var dtArr = dtStart.split('.');
//             var newDtSt = dtArr[2] + '-' + dtArr[1] + '-' + dtArr[0];
//             mckData += '{ "id" : "' + id + '", \n' +
//                 '"time" : "' + newDtSt + '", \n' +
//                 '"cls" : "' + colr + '", \n' +
//                 '"desc" : "' + wrkType + '" \n },'
//         });
//
//         mckData = mckData.substring(0, mckData.length - 1);
//         mckData += '\n ]'
//         // console.log('????????????????????????????????');
//         console.log(mckData);
//         cal.bindData(JSON.parse(mckData))
//         cal.render();
//     };
//     xhr.onprogress = function(event) {};
//     xhr.onloadend = function (ev) {};
//     xhr.onerror = function (ev) {};
//     xhr.send();
//
// });

app.initialize();
