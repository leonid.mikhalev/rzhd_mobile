/**
 * Класс построения списка запланированных работ
 */
const initialDataFolder = 'askue-mobile';
const app_con_log = 'feeder.json';

let sendreq = '';
let testDev = false; // Тестовая переменная. Если правда - работаем с сервисом Романа, если ложь - с АПИ Сергея
let wl_id = -1;   // Идентификаор плана
let baseUrl;
let team_id = -1;
let page_size = 5;
let redirect_from_conn = false;
let fConnFiles = [];
let img_ConCounter = 0; //lvm 15.07.2021;
let planImages = [];


document.addEventListener("DOMContentLoaded", function (event){
    this.team_id = localStorage.getItem('team_id');
});

$(document).ready(function() {

  $('#modalConns button.close').click()

    $('#cover').show();

    redirect_from_conn = localStorage.getItem('redirect_from_conn');
    // console.info('%c redirect_from_conn %O', 'color: red; font-size: 18px;', redirect_from_conn );

    let par1 = localStorage.getItem('baseUrl');
    let par2 = localStorage.getItem('team_id');
    let par3 = localStorage.getItem('team_status');
    let par4 = localStorage.getItem('team_plan_terms');
    let team_id = localStorage.getItem('team_id');
    let baseUrl = localStorage.getItem('baseUrl');

    let responseJson = "";
    let xhr = new XMLHttpRequest();

    let postPlanJSON = '';
    let link = baseUrl + '/api/plans?plan_team_id=' + par2;
    link = getFilterUrlWithParams(link, par3, par4);
    xhr.open('GET', link, true);
    xhr.setRequestHeader('accept', 'application/json');
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.onload = function() {
        responseJson = xhr.response;
        try {
            let jsonParsed = JSON.parse(responseJson);
            console.info('%c[INCOMING DATA]\n%O', 'color: lime;', jsonParsed);
        } catch (e) {
            console.info('%c[ERROR! INCOMING DATA]\n%O', 'color: red;', responseJson);
        }
        var cont = jsonToPlan(xhr.response);
        // console.info('%c[888 cont]\n%O', 'color: lime;', cont);
        $('#planListUL').append(cont);

        $('#cover').hide();

        // Если вернулись из connection.js открыть модальное окно с подключениями
        if ( redirect_from_conn == 'true' && redirect_from_conn !== null ) {
            redirect_from_conn = false;
            localStorage.setItem('redirect_from_conn', redirect_from_conn);
            let a1 = localStorage.getItem('worklist_frcon_id');
            let a2 = localStorage.getItem('wl_object_id');
            let a3 = localStorage.getItem('device_type'); //worklist_id getWorkListForConn

            let planObjName = localStorage.getItem('planObjName');


            // getWorkList(id, planObjName, planObjStatus, planObjStatusLoc, objectId, planComment)
            getWorkList(a1, planObjName, '','' , a2);
        }

    };
    xhr.onprogress = function(event) {};
    xhr.onloadend = function (ev) {};
    xhr.onerror = function (ev) {};
    xhr.send();

    // Постраничная подгрузка данных
    let currentPage = 1;
    let dataCount = 0;
    $('#pageCounter').val = currentPage;
    $('#dataCounter').val = dataCount;
    $(window).scroll(function () {
        var position = $(window).scrollTop();
        var bottom = $(document).height() - $(window).height();
        // Exact position == bottom doesn't work on real mobile device
        if (position >= bottom - 50) {
            currentPage++;
            appendData(currentPage, par3, par4);
        }
    });
});

/**
 * Метод догрузки плана
 * @param page
 */
function appendData(page, par3, par4) {
    var team_id = localStorage.getItem('team_id');
    var baseUrl = localStorage.getItem('baseUrl');


    par3 = localStorage.getItem('team_status');
    par4 = localStorage.getItem('team_plan_terms');

    var link = baseUrl + '/api/plans?plan_team_id=' + team_id;
    link = getFilterUrlWithParams(link, par3, par4);
    link += '&page=' + page + '&size=' + page_size;
    // if ( status == 'ALL' ) {
    //     link = baseUrl + '/api/plans?plan_team_id=' + team_id + '&page=' + page + '&size=' + page_size;
    // } else {
    //     link = baseUrl + '/api/plans?plan_team_id=' + team_id + '&status=' + status + '&page=' + page + '&size=' + page_size;
    // }

    // console.log('page = ' + page + ', status = ' + ', link = ' + link);
    var xhr = new XMLHttpRequest();
    var responseJson = "";
    xhr.open('GET', link, true);
    xhr.setRequestHeader('accept', 'application/json');
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.onload = function() {
        responseJson = xhr.response;
        var cont = jsonToPlan(xhr.response);
        $('#planListUL').append(cont);

    };
    xhr.send();
}

/**
 * Метод построения списка планов для бригады по team_id
 * @param responseJson
 * @returns {string}
 */
function jsonToPlan(responseJson) {
    var json = JSON.parse(responseJson);
    var dataCount = $('#dataCounter').val();
    var cont = '';

    $.each(json.rows, function(i, item) {
        dataCount++;
        let dtStart = item.dPlanFormatted;
        let id = item.id;
        let status = item.status;
        let wrkType = item.workType;
        let badge = 'badge-info';

        switch (status) {
            case 'SCHEDULED':
                badge = 'badge-info';
                break;
            case 'COMPLETED':
                badge = 'badge-success';
                break;
            case 'COMPLETED_WITH_REMARK':
                badge = 'badge-warning';
                break;
        }

        var colr = 'badge-info'; //bg-cyan-alt
        var sWkTp = wrkType.replace(' ', '');

        switch ( sWkTp ) {
            case 'РТО':
                colr = 'badge-info';
                break;
            case 'OTO':
                colr = 'badge-success'; //bg-orange-alt
                break;
        }

        let oStatus = ( item.statusLocalized != '' && item.statusLocalized !== null && item.statusLocalized !== undefined ) ? item.statusLocalized : 'н.д.';

        let stStyle = getPlanStatusStyle(item.status);

        let oEchName = ( item.objectEchName != '' && item.objectEchName !== null && item.objectEchName !== undefined ) ? item.objectEchName : 'н.д.';
        let oEchsName = ( item.objectEchsName != '' && item.objectEchsName !== null && item.objectEchsName !== undefined ) ? item.objectEchsName : 'н.д.';
        let oFeedNd = ( item.objectFeedingNode != '' && item.objectFeedingNode !== null && item.objectFeedingNode !== undefined ) ? item.objectFeedingNode : 'н.д.';
        let oPayTyp = ( item.objectPaymentType != '' && item.objectPaymentType !== null && item.objectPaymentType !== undefined ) ? item.objectPaymentType : 'н.д.';
        let oRegNm = ( item.objectRegionName != '' && item.objectRegionName !== null && item.objectRegionName !== undefined ) ? item.objectRegionName : 'н.д.';
        let oStNm = ( item.objectStationName != '' && item.objectStationName !== null && item.objectStationName !== undefined ) ? item.objectStationName : 'н.д.';
        let planObjName = ( item.objectStationName != '' && item.objectStationName !== null && item.objectStationName !== undefined ) ? item.objectStationName : 'н.д.';
        let planObjStatus = ( item.status != '' && item.status !== null && item.status !== undefined ) ? item.status : null;
        let planObjStatusLoc = ( item.statusLocalized != '' && item.statusLocalized !== null && item.statusLocalized !== undefined ) ? item.statusLocalized : 'н.д.';
        let planObjComment = ( item.description != '' && item.description !== null && item.description !== undefined ) ? item.description : '';

        cont +='<li class="list-group-item" onclick="getWorkList' +
            '(' + id + ',' +
            '\'' + planObjName + '\',' +
            '\'' + planObjStatus + '\',' +
            '\'' + planObjStatusLoc + '\',' +
            '\'' + item.objectId  + '\',' +
            '\'' + planObjComment + '\'' +
            ')">' + (dataCount) + '. <b>ObjectId(id): ' + item.objectId + '(' + item.id + ')</b>, ' + dtStart + ' <span class="badge ' + colr + '" >' + item.workTypeLocalized + ' </span>' +
            '<br>' +
            '<ul>' +
            '<li>Статус: <b><span style="' + stStyle + '">' + oStatus + '</span></b></li>' +
            '<li>ЭЧ: ' + oEchName + '</li>' +
            '<li>ЭЧС: ' + oEchsName + '</li>' +
            '<li>Узел: ' + oFeedNd + '</li>' +
            '<li>Сегмент: ' + oPayTyp + '</li>' +
            '<li>Регион: ' + oRegNm + '</li>' +
            '<li>Станция: ' + oStNm + '</li>' +
            '<li>Примечание: ' + planObjComment + '</li>' +
            '</ul> </li>\n';

        // LVM 31.03.2021 Счетчик данных плана
        $('#dataCounter').val(dataCount);

    });

    cont += '</ul>\n';
    return cont;
}

function getPlanStatusStyle(par1){
    switch (par1) {
        case 'IN_APPROVE':
            return 'color: #256900;'
        case 'SCHEDULED':
            return 'color: #89bf00;'
        case 'ACCEPTED':
            return 'color: #256900;'
        case 'AGREED':
            return 'color: #256900;'
        case 'IN_PROGRESS':
            return 'color: #256900;'
        case 'COMPLETED':
            return 'color: #256900;'
        case 'SUSPENDED':
            return 'color: #db7c00;'
        case 'UNKNOWN':
            return 'color: #0099db;'
    }

}

/**
 * Получения присоединений по идентификатору плана
 * @param id - идентификатор плана !!!!!!!
 * @param objectId
 '\'' + planObjName + '\',' +
 '\'' + planObjStatus + '\',' +
 '\'' + planObjStatusLoc + '\',' +*
 */
function getWorkList(id, planObjName, planObjStatus, planObjStatusLoc, objectId, planComment) {
    $('#cover').show();
    wl_id = id;
    console.warn('id=' + id);
    $('#connList').empty();
    this.baseUrl = localStorage.getItem('baseUrl');

    var xhr = new XMLHttpRequest();
    var responseJson = "";

    xhr.open('GET', this.baseUrl + '/api/plan_details?plan_id=' + id, true);
    console.log("%c[SENDING GET]: %O/api/plan_details?plan_id=%O", 'color:lime;', this.baseUrl, id);
    xhr.setRequestHeader('accept', 'application/json');
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.onload = function() {
        responseJson = xhr.response;
        var json = JSON.parse(responseJson);
        console.info('%c[ACQUIRING GET RESPONSE]\nURL: %O\nDATA: %O', 'color: lime;', xhr.responseURL, json);
        var html = '';
        var planDesc = ''
        $.each(json.rows, function (i, item) {
            var statusBadge='';
            if ( item.status) {
                switch (item.status) {
                    case 'SCHEDULED':
                        statusBadge = '<span class="badge bg-primary" style="color: white;">Запланирован (или утвержден) диспетчером</span><br>';
                        break;
                    case 'IN_APPROVE':
                        statusBadge='<span class="badge bg-success" style="color: white;">Бригада добавила объект в план работ</span><br>';
                        break;
                    case 'ACCEPTED':
                        statusBadge='<span class="badge bg-info text-dark">Бригада взяла план в работу</span><br>';
                        break;
                    case 'AGREED':
                        statusBadge='<span class="badge bg-info text-dark">Выезд согласован (бригадой)</span><br>';
                        break;
                    case 'IN_PROGRESS':
                        statusBadge='<span class="badge bg-success" style="color: white;">План работ (на объекте) в процессе выполнения</span><br>';
                        break;
                    case 'COMPLETED':
                        statusBadge='<span class="badge bg-success" style="color: white;">Завершён</span><br>';
                        break;
                    case 'SUSPENDED':
                        statusBadge='<span class="badge bg-danger" style="color: white;">Работа над планом приостановлена</span><br>';
                        break;
                    case 'COMPLETED_WITH_REMARK':
                        statusBadge='<span class="badge bg-warning">Завершено с замечаниями</span><br>';
                        break;
                }
            }

            html += '<li ' +
                'class="list-group-item" onclick="getWorkListForConn(' + id + ', \'' + btoa(encodeURI(JSON.stringify(item))) + '\')">' +
                '' + '<b>' + (i + 1) + '. ' + item.dPlanFormatted + '</b><br>' +
                '' + statusBadge +
                '' + item.device.name + '<br>' +
                '</li>\n';
            planDesc = item.plan.description;
        });
        $('#tPlanComment').val('');
        if ( planComment === undefined ) planComment = planDesc;
        $('#tPlanComment').val(planComment);

        $('#connList').append(html);
        $('#cover').hide();

        try {
            if ( cordova ) {
                try {
                    connNetStResolveFS();
                } catch (e) {
                    console.info('%c Error resolving file system:\n%O', 'color: red;', e);
                }
            }
        } catch (e) {
            console.info('%c[INFO] Cordova is not defined, probably no device application running', 'color: white;');
        }

    };
    xhr.onprogress = function(event) {};
    xhr.onloadend = function (ev) {};
    xhr.onerror = function (ev) {
      console.log('%cERROR ZZZ \n%O', 'color:red;', ev);

    };
    xhr.send();

    // -----------------------------------------------------------------------------------------------------------------
    // -----------------------------------------------------------------------------------------------------------------
    // -----------------------------------------------------------------------------------------------------------------

    var planItemsJsonDecoded = null;

    try {

        planItemsJsonDecoded = JSON
            .parse(
                decodeURI(
                    atob(
                        localStorage.getItem("planItemsJsonEncoded"))
                )
            );
        console.info('%c [IMAGE DATA FROM STORAGE]\n%O', 'color: #e6e872;', planItemsJsonDecoded);

        $('#tConnLogPics').empty();
        fConnFiles = [];
        img_ConCounter = 0;
        planImages = planItemsJsonDecoded;

        if ( planImages ) {
            let imgTmplt = '';
            $.each(planImages, function (arrIndex, data) {
                console.info('%cplanId: %O; data: %O', 'color: orangered;', arrIndex, data);
                if ( data.planId == wl_id ) {
                    console.info('%cdata.data: %O', 'color: orangered;', data.data);

                    var countDataArr = data.data;
                    // countDataArr.sort();
                    countDataArr.sort((a,b) => {
                        if ( b.imgCount < a.imgCount ) return -1;
                        if ( b.imgCount > a.imgCount ) return 1;
                        return 0;
                    });
                    console.info('%ccountDataArr: %O', 'color: red;', countDataArr);
                    console.info('%ccountDataArr[0].imgCount: %O', 'color: red;', countDataArr[0].imgCount);
                    img_ConCounter = countDataArr[0].imgCount + 1;

                    // /sdcard//Android/data/com.system.location/askue-mobile/15_7_2021_15_48_21.596.png
                    $.each(data.data, function (dataIndex, dataValue) {
                        console.info('%cimgCounter: %O; fileSrc: %O', 'color: orangered;', dataValue.imgCount, dataValue.src);


                        let cf = new ConnectionPhoto(dataValue.imgCount, dataValue.src);
                        fConnFiles.push(cf); // Не забыть удалить из массива, в случае fileWriterError

                        imgTmplt +=
                            '<li class="list-group-item" id="li_con_' + dataValue.imgCount + '">\n' +
                            '    <img class="card-img-top" src="' + dataValue.src + '">\n' +
                            '    <div class="d-flex flex-row-reverse" style="padding-top: 10px;" id="img_' + dataValue.imgCount + '">\n' +
                            '        <button type="button" class="btn btn-outline-danger" onclick="remFromConList(' + dataValue.imgCount + ')" style="margin-left: 3px;">Удалить</button>\n' +
                            '    </div>\n' +
                            '</li>\n';


                        if ( dataValue.imgCount == img_ConCounter ) {
                        }


                    });

                }
            });
            $('#tConnLogPics').append(imgTmplt);


        }
        // console.info('%c[IMAGES DATA]:\n%O', 'color: yellow;', planImages);
        // var planItemsJsonEncoded = btoa(encodeURI(JSON.stringify(planImages)));
        // localStorage.setItem("planItemsJsonEncoded", planItemsJsonEncoded);



    } catch (e) {
        console.info('%c [IMAGE DATA FROM STORAGE] EMPTY', 'color: orangered;');
        console.info(e);
    }
    // -----------------------------------------------------------------------------------------------------------------
    // -----------------------------------------------------------------------------------------------------------------
    // -----------------------------------------------------------------------------------------------------------------



    $('#modalConns').modal('toggle');

    $('#modalConns').on('shown.bs.modal', function(event) {
        // Feeder list
        let $rowData = $("#connList li");
        let scroll_to = localStorage.getItem('feeder_id_scroll');
        let targetId  = 'feeder_' + scroll_to;
        $.each($rowData, function(i, item) {
            if ( targetId == item.id ) {
                location.href = "#";
                location.href = '#'+item.id;
                localStorage.setItem( 'feeder_id_scroll', null );
            }
        });
    });
}

/**
 * Срок, сегодня, на месяц, на месяц(текущий) на неделю, на неделю(текщую)
 * @param par1
 */
function setTeamTerms(par1) {
    switch (par1) {
        case 'all':
            document.getElementById("bSelectTerms").value = 'Все';
            document.getElementById("bSelectTerms").innerText = 'Все';
            break;
        case 'today':
            document.getElementById("bSelectTerms").value = 'На сегодня';
            document.getElementById("bSelectTerms").innerText = 'На сегодня';
            break;
        case 'on_week':
            document.getElementById("bSelectTerms").value = 'На неделю начиная с сегодня';
            document.getElementById("bSelectTerms").innerText = 'На неделю начиная с сегодня';
            break;
        case 'all_week':
            document.getElementById("bSelectTerms").value = 'На неделю начиная с начала недели';
            document.getElementById("bSelectTerms").innerText = 'На неделю начиная с начала недели';
            break;
        case 'on_month':
            document.getElementById("bSelectTerms").value = 'На текущий месяц начиная с сегодня';
            document.getElementById("bSelectTerms").innerText = 'На текущий месяц начиная с сегодня';
            break;
        case 'all_month':
            document.getElementById("bSelectTerms").value = 'На весь текущий месяц';
            document.getElementById("bSelectTerms").innerText = 'На весь текущий месяц';
            break;
        default:
            par1 = 'all';
            document.getElementById("bSelectTerms").value = 'Все';
            document.getElementById("bSelectTerms").innerText = 'Все';
            break;
    }
    localStorage.setItem('team_plan_terms', par1);
}

/**
 * Метод установки выбранного статуса плана бригады
 * @param par1
 * IN_APPROVE | SCHEDULED | ACCEPTED | AGREED | IN_PROGRESS | COMPLETED | SUSPENDED | UNKNOWN
 */
function setTeamStatus(par1) {
    switch (par1) {
        case 'ALL':
            document.getElementById("bPlanStatus").value = 'Все';
            document.getElementById("bPlanStatus").innerText = 'Все';
            break;
        case 'SCHEDULED':
            document.getElementById("bPlanStatus").value = 'Запланировано';
            document.getElementById("bPlanStatus").innerText = 'Запланировано';
            break;
        case 'COMPLETED':
            document.getElementById("bPlanStatus").value = 'Завершено';
            document.getElementById("bPlanStatus").innerText = 'Завершено';
            break;
        case 'COMPLETED_WITH_REMARK':
            document.getElementById("bPlanStatus").value = 'Завершено с замечаниями';
            document.getElementById("bPlanStatus").innerText = 'Завершено с замечаниями';
            break;
        case 'SUSPENDED':
            document.getElementById("bPlanStatus").value = 'Приостановлено';
            document.getElementById("bPlanStatus").innerText = 'Приостановлено';
            break;
        case 'IN_APPROVE':
            document.getElementById("bPlanStatus").value = 'На утверждении';
            document.getElementById("bPlanStatus").innerText = 'На утверждении';
            break;
        case 'ACCEPTED':
            document.getElementById("bPlanStatus").value = 'Принято';
            document.getElementById("bPlanStatus").innerText = 'Принято';
            break;
        case 'AGREED':
            document.getElementById("bPlanStatus").value = 'Согласовано';
            document.getElementById("bPlanStatus").innerText = 'Согласовано';
            break;
        case 'IN_PROGRESS':
            document.getElementById("bPlanStatus").value = 'В работе';
            document.getElementById("bPlanStatus").innerText = 'В работе';
            break;
        case 'UNKNOWN':
            document.getElementById("bPlanStatus").value = 'Не определено';
            document.getElementById("bPlanStatus").innerText = 'Не определено';
            break;

    }
    localStorage.setItem('team_status', par1);
}

function getFilterUrlWithParams(baseLink, par3, par4 ) {

    console.info("%cBaseLink: %O;\npar3: %O;\npar4: %O", 'color: orangered;', baseLink, par3, par4);

    setTeamStatus(par3);
    setTeamTerms(par4);

    var link = baseLink;
    let tm_days_period = '';
    let tm_status = '';

    if ( par3 != 'ALL' ) { tm_status = '&status=' + par3; }

    switch (par4) {
        case 'today':
            tm_days_period = '&days_period=0:0';
            break;
        case 'on_week':
            tm_days_period = '&days_plus=7';
            break;
        case 'all_week':
            let todayInt = new Date().getDay() + 1;
            let dayStart = 0, dayEnd = 0;
            if ( todayInt > 1 && todayInt < 7) {
                dayStart = (todayInt - 1)*(-1);
                dayEnd = 7 - todayInt;
            }
            if ( todayInt == 1 ){ dayStart = 0; dayEnd = 7; }
            if ( todayInt == 7 ){ dayStart = -7; dayEnd = 0; }
            tm_days_period = `&days_period=${dayStart}:${dayEnd}`;
            break;
        case 'on_month':
            var todayDate = new Date().getDate();
            var daysCount = function(month,year) {
                return new Date(year, month, 0).getDate();
            };
            tm_days_period = `&days_period=0:${daysCount(new Date().getMonth(), new Date().getFullYear())-todayDate}`;
            break;
        case 'all_month':
            var todayDate = new Date().getDate()*(-1);
            var daysCount = function(month,year) {
                return new Date(year, month, 0).getDate();
            };
            tm_days_period = `&days_period=${todayDate}:${daysCount(new Date().getMonth(), new Date().getFullYear())+todayDate}`;
            break;
    }
    link += tm_status + tm_days_period;
    return link;
}

function applyPlanFilter() {
    $('#cover').show();
    let a1 = localStorage.getItem('team_status');
    let a2 = localStorage.getItem('team_plan_terms');
    updatePlanWithFilter();
}

function updatePlanWithFilter() {

    localStorage.setItem('redirect_from_conn', false);

    let par1 = localStorage.getItem('baseUrl');
    let par2 = localStorage.getItem('team_id');
    let par3 = localStorage.getItem('team_status');
    let par4 = localStorage.getItem('team_plan_terms');
    let team_id = localStorage.getItem('team_id');
    let baseUrl = localStorage.getItem('baseUrl');

    var link = getFilterUrlWithParams( baseUrl + '/api/plans?plan_team_id=' + par2, par3, par4);

    let responseJson = "";
    let xhr = new XMLHttpRequest();
    xhr.open('GET', link, true);
    console.log(link);
    xhr.setRequestHeader('Access-Control-Allow-Origin', '*');
    xhr.setRequestHeader('accept', 'application/json');
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.onload = function() {

        let currentPage = 1;
        let dataCount = 0;
        $('#pageCounter').val = currentPage;
        $('#dataCounter').val(0);

        responseJson = xhr.response;
        let cont = jsonToPlan(xhr.response);
        $('#planListUL').empty();
        $('#planListUL').append(cont);
        $('#cover').hide();

    };
    xhr.onprogress = function(event) {};
    xhr.onloadend = function (ev) {};
    xhr.onerror = function (ev) {
        $('#cover').hide();
      console.log('%cERROR\n%O', 'color:red;', ev);
    };
    xhr.send();
}

/**
 * Метод перенаправления на форму заполнения данных работы в присоединении
 * @param id         - идентификатор плана
 * @param objectId   - какая-то хуйня
 * @param connId     - идентификатор присоединения
 * @param connName   - названия присоединения
 * @param connStMsg  - Расшифровка статуса ( ПОКА ХАРДКОД, НАДО ЧИТАТЬ ИЗ JSON )
 * @param connSts    - Статус присоединения
 * @param conVal     - Показания ( по умолчанию пустая строка )
 * @param conComment - Примечания ( по умолчанию пустая строка )
 */
function getWorkListForConn(id, item) {
    var jsonItem = decodeURI(atob(item));
    console.info('%c[ITEM PASSED TO WLFORCONN]:\n%O', 'color: orangered;', JSON.parse(jsonItem));
    localStorage.setItem('worklist_id', id);
    localStorage.setItem('worklist_item', item);
    $(location).attr('href','connection.html');
}

// [START] -------------------------------------------------------------------------------------------------------------
/**
 * Запуск цепочки создания/чтения/редактирования
 * файла состояния отправки подключения
 */
function connNetStResolveFS() {
    window.resolveLocalFileSystemURL(cordova.file.externalApplicationStorageDirectory, createConnNetStFolder, createConnNetStFolderErr);
}

/**
 * Создние папки(папка по умолчанию) [SUCCESS CALLBACK]
 */
function createConnNetStFolder(folder) {
    folder.getDirectory(initialDataFolder, { create: true}, createConnNetStFile, e1);
}

/**
 * Создание папки(папка по умолчанию) [SUCCESS CALLBACK]
 */
function createConnNetStFolderErr(folder) {
    folder.getDirectory(initialDataFolder, { create: false}, createConnNetStFile, e2);
}

/**
 * Создание файла(Если нет файла лоша отправки, или ничего
 * не делать, если файл есть) [SUCCESS CALLBACK]
 * @param folder
 */
function createConnNetStFile(folder) {
    folder.getFile(app_con_log, { create: true }, createConnNetStFileSuccess, createFileError);
}

/**
 * Создание fileWriter'а для лога статуса отправки
 * состояния подключения [SUCCESS CALLBACK]
 * @param file
 */
function createConnNetStFileSuccess(fileEntry) {
    // LVM
    //  1. Зачитать
    //  2. Проверить массив на значения isSent = true
    //  3. Почистить массив, где есть п.2
    //  4. Сформировать новый JSON с учетом записей пп. 1 - 3
    //  5. Записать новые данные
    fileEntry.file(
        function (file) {
            let reader = new FileReader();
            reader.onloadend = function(dev)
            {
                console.log("Successful file read: " + this.result);
                console.log(this.result);

                if ( this.result != '' && this.result !== null ) {
                    $('#bSaveUnsent').prop('disabled', false);


                    if (sendreq == 'sendreq') {
                        sendreq = '';

                        let foundData = JSON.parse(this.result);
                        $.each(foundData.data, function(i, item) {
                            // LVM PROMISE PROMISE PROMISE PROMISE PROMISE PROMISE PROMISE
                            devSendConnIssueUnsent(item);
                        });

                    }

                } else {
                    $('#bSaveUnsent').prop('disabled', true);
                }
            };
            reader.readAsText(file);
        },
        function error(e) {
            console.log("Error read connection log file!");
            console.log(e);
        }
    );

}

/**
 * Закрыть/Закрыть с замечаниями задание в подключении
 */
function closePlanIssue(par) {

    console.info('%c[DEBUG] Method involved: closePlanIssue(%O) - (TRUE: Завершить, FALSE: Приостановить)', 'color: orange;', par);

    $('#cover').show();

    var keys = Object.keys(localStorage);
    console.info('%c [LOCAL STORAGE ITEMS] LocalStorage:\n%O', 'color: orange;', keys);
    $.each(keys, function(k, v) {
       console.info('%O, %O', v, localStorage.getItem(v));
    });
    var issueObject = {"id": null, "description": null, "performedTeam" : {"id" : null} };

    localStorage.setItem("worklist_id", wl_id);
    var planId = localStorage.getItem("worklist_id");
    var teamId = localStorage.getItem("team_id");
    var comment = $('#tPlanComment').val();
    var baseUrl = localStorage.getItem("baseUrl");
    var action = ( par == true ) ? 'complete' : 'suspend' ;
    issueObject.id = planId;
    issueObject.description = comment;
    issueObject.performedTeam.id = teamId;

    var postURL = baseUrl + '/api/plans/' + action;

    console.info('%c[SENDING POST REQUEST (%O)] \nURL:%O\nissueObject: %O', 'color: orange;', postURL, action, issueObject);
    let xhr = new XMLHttpRequest();
    let responseJson = "";
    xhr.open('POST', postURL, true);
    xhr.setRequestHeader('accept', 'application/json');
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.onload = function()
    {
        responseJson = xhr.response;
        console.log(responseJson);
        var json = JSON.parse(responseJson);
        console.log(json);
        $('#modalConns').modal('toggle');

        let sCount = 0;
        $.each(fConnFiles, function (i, item) {

            var uri = encodeURI(localStorage.getItem('baseUrl') + '/api/photos/upload/one');
            var params = {};
            params.plan_id = wl_id;
            params.type = 'JOURNAL';

            var options = new FileUploadOptions();
            options.params = params;

            var ft = new FileTransfer();
            var f1 = $.extend(f1, item.file);

            ft.upload(
                item.file,
                uri,
                function(r) {
                    console.log(r);
                    console.log("Code = " + r.responseCode);
                    console.log("Response = " + r.response);
                    console.log("Sent = " + r.bytesSent);
                    console.log("sendCounter: " + sCount + " (" + fConnFiles.length + ")");
                    console.log("==============================")
                    sCount ++;
                    if ( sCount == fConnFiles.length) {
                        // todo redirect ...
                        $('#cover').hide();
                        console.log('REDIRECT FROM SUCCESS!!!!!!!!!!!!!!!!!!!!!!!!!!');
                        localStorage.setItem('redirect_from_conn', false);
                        $(location).attr('href', 'plan.html');
                    }

                },
                function(error) {
                    sCount ++;
                    console.error(error);
                    console.log("sendCounter: " + sCount + " (" + fConnFiles.length + ")");
                    if ( sCount == fConnFiles.length) {
                        // todo redirect ...
                        $('#cover').hide();
                        console.log('REDIRECT FROM ERROR!!!!!!!!!!!!!!!!!!!!!!!!!!');
                        alert("При отправке снимков произошла ошибка!")
                        localStorage.setItem('redirect_from_conn', false);
                        $(location).attr('href', 'plan.html');
                    }
                },
                options);

        });
        $('#cover').hide();
        //location.reload();

    };

    xhr.onprogress = function(event) {};
    xhr.onloadend = function (ev) {};
    xhr.onerror = function (ev) {};
    xhr.send(JSON.stringify(issueObject));

    $('#cover').hide();
}

/**
 *
 * @param par true/false COMPLETED/C`OMPLETED_WITH_REMARK
 */
function devSendConnIssue(par){}

/**
 *
 * @param par true/false COMPLETED/COMPLETED_WITH_REMARK
 */
function devSendConnIssueUnsent(par){}

// <!-- CAMERA JOURNAL -->

/**
 * Инициализация камеры
 */
function getJrnlPicture() {

    $('#cover').show();
    
    try {
        if ( cordova ) {
            console.log(cordova.plugins.permissions);

            let options = {
                quality: 100,
                destinationType: Camera.DestinationType.DATA_URL,
                sourceType: Camera.PictureSourceType.CAMERA,
                mediaType: Camera.MediaType.PICTURE,
                encodingType: Camera.EncodingType.JPG,
                cameraDirection: Camera.Direction.BACK,
                correctOrientation: 1,
                targetWidth: 1440,
                targetHeight: 2037
            };
            try {
                navigator.camera.getPicture(cameraSuccess, cameraError, options);
            } catch (e) {
                $('#cover').hide();
                console.info('%c[ERROR] Method::getJrnlPicture.\n%O', 'color: red;', e);
            }
        }
    } catch (e) {
        $('#cover').hide();

        console.info('%c[INFO] Method::getJrnlPicture. Cordova is not defined, probably no device application running', 'color: white;');
    }
}

/**
 * Получение блоба снимка [SUCCESS CALLBACK]
 * @param imgSrc
 */
function cameraSuccess(imgSrc) {
    imgData = imgSrc;
    window.resolveLocalFileSystemURL(cordova.file.externalApplicationStorageDirectory, createFolder, folderExists);
}

/**
 * Создние папки [SUCCESS CALLBACK]
 */
function createFolder(folder) {
    folder.getDirectory(initialDataFolder, { create: true}, createFile, e1);
}

/**
 * Создание папки [SUCCESS CALLBACK]
 */
function folderExists(folder) {
    folder.getDirectory(initialDataFolder, { create: false}, createFile, e2);
}

/**
 * Создание файла [SUCCESS CALLBACK]
 * @param folder
 */
function createFile(folder) {
    let file = getDate('_') + '.png';
    folder.getFile(file, { create: true }, createFileSuccess, createFileError);
}

/**
 * ДЛЯ РАЗРАБОТКИ
 * Местный вспомогательный метод получения строки из даты
 * @param par1 - delimiter
 * @returns {string}
 */
function getDate(par1) {
    let now = new Date();
    let y  = now.getFullYear();
    let m  = now.getMonth() + 1;
    let d  = now.getDate();
    let hh = now.getHours();
    let mm = now.getMinutes();
    let ss = now.getSeconds();
    let ms = now.getMilliseconds();

    let retVal =
        d  + par1 +
        m  + par1 +
        y  + par1 +
        hh + par1 +
        mm + par1 +
        ss + '.' + ms;

    return retVal;
}

/**
 * Создание файла [SUCCESS CALLBACK]
 * @param folder
 */
function createFile(folder) {
    let file = getDate('_') + '.png';
    folder.getFile(file, { create: true }, createFileSuccess, createFileError);
}

/**
 * Создание fileWriter'а [SUCCESS CALLBACK]
 * @param file
 */
function createFileSuccess(file) {
    console.info('%c file.fullpath: %O', 'color: lime;', file.fullPath);
    let cf = new ConnectionPhoto(img_ConCounter, '/sdcard/'+file.fullPath);
    fConnFiles.push(cf); // Не забыть удалить из массива, в случае fileWriterError
    console.info('%c fConnFiles: %O', 'color: yellow;', fConnFiles);

    if ( planImages ) {
        var newPlanEntry = true;
        $.each(planImages, function (arrIndex, data) {
           console.info('%cplanId: %O; data: %O', 'color: orangered;', arrIndex, data);
           if ( data.planId == wl_id ) {
               newPlanEntry = false;
               var newImgEntry = true;
               console.info('%cdata.data: %O', 'color: orangered;', data.data);
               $.each(data.data, function (dataIndex, dataValue) {
                   console.info('%cimgCounter: %O; fileSrc: %O', 'color: orangered;', dataValue.imgCount, dataValue.src);
                   if ( dataValue.imgCount == img_ConCounter ) {
                       var newImgEntry = false;
                       dataValue.src = file.fullPath;
                   }
               });
               if ( newImgEntry ) {
                   data.data.push({ "imgCount": img_ConCounter, "src": '/sdcard/' + file.fullPath });
               }
           }
        });
        if ( newPlanEntry ) {
            var item = { "planId": wl_id, "data" : [{"imgCount": img_ConCounter, "src": '/sdcard/' + file.fullPath}]};
            planImages.push(item);
        }


    }
    console.info('%c[IMAGES DATA]:\n%O', 'color: yellow;', planImages);
    var planItemsJsonEncoded = btoa(encodeURI(JSON.stringify(planImages)));
    localStorage.setItem("planItemsJsonEncoded", planItemsJsonEncoded);
    // var jsonItem = decodeURI(atob(item));
    // var connData = JSON.parse(jsonItem);


    file.createWriter(fileWriterSuccess, fileWriterError);
}

/**
 * Запись изображения в файл [SUCCESS CALLBACK]
 * @param fileWriter
 */
function fileWriterSuccess(fileWriter) {
    console.log(fileWriter);
    fileWriter.seek(fileWriter.length);
    let blob = b64toBlob(imgData, 'image/png', 512);
    fileWriter.onwrite = function (e) {
        console.log("WRITE SUCCESS");
        console.log(e);
        updateConList();
        img_ConCounter ++;
    }
    fileWriter.write(blob);
}

/**
 * Добавить шаблон списка снимка прибора к форме
 */
function updateConList(){

    let imgTmplt = '';
    $.each(fConnFiles, function(i, item) {
        if ( item.id == img_ConCounter ) {
            imgTmplt +=
                '<li class="list-group-item" id="li_con_' + img_ConCounter + '">\n' +
                '    <img class="card-img-top" src="' + item.file + '">\n' +
                '    <div class="d-flex flex-row-reverse" style="padding-top: 10px;" id="img_' + img_ConCounter + '">\n' +
                '        <button type="button" class="btn btn-outline-danger" onclick="remFromConList(' + img_ConCounter + ')" style="margin-left: 3px;">Удалить</button>\n' +
                '    </div>\n' +
                '</li>\n';

            // lvm Retrieve file from fileEntry example
            // try {
            //   item.file.file(function(f){ //this does the trick
            //     console.log(f);
            //   });
            // }
            // catch(err) { console.error(err); }

        }
    });

    $('#tConnLogPics').append(imgTmplt);
    $('#cover').hide();
}

/**
 * Удалить снимок прибора из списка
 * @param par
 */
function remFromConList(par) {
    // Удаляем из списка в DOM'е
    let elem = document.getElementById('li_con_' + par);
    elem.parentElement.removeChild(elem);

    console.info('%c[fConnFiles]:\n%O', 'color: red;', fConnFiles);

    // Удаляем из массива
    $.each(fConnFiles, function(i, item) {
        if ( item.id == par ) {
            const index = fConnFiles.indexOf(item);
            if ( index > -1 ) {
                fConnFiles.splice(index, 1); return false;
            }
        }
    });

    var planItemsJsonDecoded = JSON
        .parse(
            decodeURI(
                atob(
                    localStorage.getItem("planItemsJsonEncoded"))
            )
        );


    // console.info('%c []', 'color: lime;', );
    console.info('%c [planItemsJsonDecoded]\n%O', 'color: lime;', planItemsJsonDecoded);
    console.info('%c [wl_id]: %O', 'color: lime;', wl_id);
    console.info('%c [par]: %O', 'color: lime;', par);


    $.each(planItemsJsonDecoded, function(index, data) {
       console.info('%c --> index: %O; data.planId: %O; data: %O', 'color: yellow;', index, data.planId, data);
       if ( data.planId == wl_id ) {
           $.each(data.data, function(i, v) {
                console.info('%c -----> i: %O; v.imgCount: %O; v: %O', 'color: #eced9f;', i, v.imgCount, v);
                if ( v.imgCount == par ) {
                    console.info('%c DELETING ENTRY: %O', 'color: red;', v);
                    data.data.splice(i, 1); return false;
                }
           });
       }
    });
    planImages = planItemsJsonDecoded;
    console.info('%c[IMAGES DATA planItemsJsonDecoded]:\n%O', 'color: cyan;', planItemsJsonDecoded);
    console.info('%c[IMAGES DATA planItemsJsonDecoded]:\n%O', 'color: magenta;', planImages);
    var planItemsJsonEncoded = btoa(encodeURI(JSON.stringify(planImages)));
    localStorage.setItem("planItemsJsonEncoded", planItemsJsonEncoded);
    // var jsonItem = decodeURI(atob(item));
    // var connData = JSON.parse(jsonItem);

    // var jsonItem = decodeURI(atob(item));
    // var connData = JSON.parse(jsonItem);






}

/**
 * Метод отправки файла в АПИ на сервер
 * @param fileEntry
 */
function uploadFile(fileEntry) {
    var uri = encodeURI(localStorage.getItem('baseUrl') + '/api/photos/upload/one');
    // uri = 'https://webhook.site/04bc2b67-5a0a-4b9b-8430-58cb427214a1';
    console.log(fileEntry);
    console.log("PLAN IDENTITY : " + wl_id);

    var params = {};
    params.plan_id = wl_id;
    params.type = 'JOURNAL';

    var options = new FileUploadOptions();
    options.params = params;

    var ft = new FileTransfer();
    ft.upload(
        fileEntry.fullPath,
        uri,
        function(r) {
            console.log(r);
            console.log("Code = " + r.responseCode);
            console.log("Response = " + r.response);
            console.log("Sent = " + r.bytesSent);
        },
        function(error) {
            console.log(error);
        },
        options);
    // ft.onProgress((progressEvent) => {
    //   this.events.publish("updater:download", {progress: Math.round(((progressEvent.loaded / progressEvent.total) * 100))});
    // });
}

/**
 * Коневертация BASE64 в бинарник
 * @param b64Data
 * @param contentType
 * @param sliceSize
 * @returns {Blob}
 */
function b64toBlob(b64Data, contentType, sliceSize) {
    contentType = contentType || '';
    sliceSize = sliceSize || 512;

    let byteCharacters = atob(b64Data);
    let byteArrays = [];

    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        let slice = byteCharacters.slice(offset, offset + sliceSize);

        let byteNumbers = new Array(slice.length);
        for (let i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
        }

        let byteArray = new Uint8Array(byteNumbers);

        byteArrays.push(byteArray);
    }

    let blob = new Blob(byteArrays, {type: contentType});
    return blob;
}

/**
 * Модель снимка прибора
 * @param id   - счетчик в DOM'е (img_ConCounter)
 * @param file - объект FileEntry(Cordova)
 * @constructor
 */
function ConnectionPhoto(id, filePath){
    this.id = id;
    this.file = filePath;
}

function resetFeederStats() {
        if ( wl_id < 0 ) return;
        $('#connList').empty();

        let xhr = new XMLHttpRequest();
        let responseJson = "";
        xhr.open('POST', 'http://192.168.2.40:3000/rpc/clear_em_work_list', true);
        xhr.onreadystatechange = function() {
            if (xhr.readyState === 2) {}
        }
        xhr.setRequestHeader('accept', 'application/json');
        xhr.setRequestHeader('Content-Type', 'application/json');
        xhr.onload = function()
                     {
                        responseJson = xhr.response;
                        var json = JSON.parse(responseJson);
                        console.log(json);
                        $('#modalConns').modal('toggle');
                     };

        xhr.onprogress = function(event) {};
        xhr.onloadend = function (ev) {};
        xhr.onerror = function (ev) {};
        let jsMsg = '{"params": { "id_plan": "' + wl_id + '"} }';
        xhr.send(jsMsg);
}

function sendUnsent(par){
    sendreq = par;
    connNetStResolveFS();
}

// <!-- ERROR CALLBACKS -->

function cameraError(e) {
    $('#cover').hide();
    console.log('ERROR! CAMERA FAILED!');
    console.log(e);
}

function e1(e) {
    $('#cover').hide();
    console.log('ERROR! createFolder()!');
    console.log(e);
}

function e2(e) {
    $('#cover').hide();
    console.log('ERROR! folderExists()!');
    console.log(e);
}

function createFileError(e) {
    console.log('ERROR! createFile()!');
    console.log(e);
}

function fileWriterError(e) {
    $('#cover').hide();
    console.log('ERROR! fileWriterError()!');
    console.log(e);
    fConnFiles.slice(-1, 1); // Удаляем последнюю добавленную запись из массива файлов
}
